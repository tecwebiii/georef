-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.49-0ubuntu0.12.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table geoplaces.fotos
CREATE TABLE IF NOT EXISTS `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ponto_id` int(10) unsigned DEFAULT NULL,
  `ficheiro` varchar(150) NOT NULL,
  `historia` text NOT NULL,
  `utilizador_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fotos_ponto` (`ponto_id`),
  KEY `FK_fotos_utilizador` (`utilizador_id`),
  CONSTRAINT `FK_fotos_ponto` FOREIGN KEY (`ponto_id`) REFERENCES `ponto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_fotos_utilizador` FOREIGN KEY (`utilizador_id`) REFERENCES `utilizador` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- Dumping data for table geoplaces.fotos: ~53 rows (approximately)
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` (`id`, `ponto_id`, `ficheiro`, `historia`, `utilizador_id`) VALUES
	(35, 5, 'madein21.jpg', 'ola', 2),
	(36, 5, 'images (2).jpg', 'Rafael Bordalo Pinheiro', 2),
	(39, 6, 'bordallo-logo-300x300.jpg', '', 2),
	(40, 5, 'images.jpg', '', 2),
	(41, 5, 'RafaelBordaloPinheiro.jpg', '', 2),
	(42, 5, 'images (1).jpg', '', 2),
	(43, 5, 'bordallo-logo-300x300.jpg', '', 2),
	(44, 5, 'MuseuBordaloPinheiro-ConVida2015-0200.jpg', '', 2),
	(45, 5, 'Rafael-Bordalo-Pinheiro.jpg', '', 2),
	(46, 6, 'flower.jpg', '', 2),
	(47, 6, 'night.jpg', '', 2),
	(48, 6, 'flowers.jpg', '', 2),
	(49, 6, 'desert.jpg', '', 2),
	(50, 6, 'sunset.jpg', '', 2),
	(51, 6, 'surf.jpg', '', 2),
	(52, 6, 'butterfly.jpg', '', 2),
	(53, 6, 'city.jpg', '', 2),
	(54, 6, 'tiger.jpg', '', 2),
	(57, 6, 'water.jpg', '', 2),
	(58, 61, 'pinto.jpg', '', 2),
	(59, 61, 'image175.jpg', '', 2),
	(60, 6, 'color Corectionspng.png', '', 2),
	(61, NULL, '187598484_10 (1).jpg', '', 2),
	(62, NULL, 'transferir.png', '', 2),
	(63, 65, 'IMG_20150913_105037_1.jpg', '', 2),
	(65, 25, 'desert.jpg', '', 2),
	(66, 25, 'butterfly.jpg', '', 2),
	(67, 25, 'night.jpg', '', 2),
	(68, 25, 'flowers.jpg', '', 2),
	(69, 25, 'flower.jpg', '', 2),
	(70, 25, 'city.jpg', '', 2),
	(71, 25, 'surf.jpg', '', 2),
	(72, 25, 'tiger.jpg', '', 2),
	(73, 25, 'sunset.jpg', '', 2),
	(74, 106, 'desert.jpg', '', 2),
	(75, 106, 'city.jpg', '', 2),
	(76, 106, 'flower.jpg', '', 2),
	(77, 107, 'night.jpg', '', 2),
	(78, 107, 'butterfly.jpg', '', 2),
	(79, 63, 'myfoto.png', '', 2),
	(80, 63, 'IMG_20160426_112022.jpg', '', 2),
	(81, 70, 'surf.jpg', '', 2),
	(82, 33, 'tiger.jpg', '', 2),
	(83, 81, 'night.jpg', '', 2),
	(84, 101, 'flower.jpg', '', 2),
	(85, 113, 'CIMG1054.JPG', '', 2),
	(86, 113, 'IMG_20150913_105037_1.jpg', '', 2),
	(87, 84, 'butterfly.jpg', '', 2),
	(88, 124, 'desert.jpg', '', 2),
	(89, 25, '16.jpg', '', 2),
	(90, 144, 'ovo5.jpg', '', 2),
	(91, 144, '27.jpg', '', 2),
	(92, 81, 'ovo2.jpg', '', 2),
	(93, 81, '08.jpg', '', 2),
	(94, 5, 'CIMG0807.JPG', '', 2),
	(95, 61, 'CIMG0813.JPG', '', 2),
	(96, 6, 'CIMG0813.JPG', '', 2),
	(97, 123, 'CIMG0813.JPG', '', 2),
	(98, 5, 'CIMG0810.JPG', '', 2),
	(99, 63, 'CIMG0813.JPG', '', 2),
	(100, 64, 'CIMG0807.JPG', '', 2),
	(101, 63, 'CIMG0807.JPG', '', 2),
	(102, 64, 'CIMG0821.JPG', '', 2),
	(103, 65, 'CIMG0813.JPG', '', 2);
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;


-- Dumping structure for table geoplaces.ponto
CREATE TABLE IF NOT EXISTS `ponto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `icon` varchar(30) DEFAULT '',
  `rota_id` int(10) unsigned NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `historia` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_point_rota` (`rota_id`),
  CONSTRAINT `FK_point_rota` FOREIGN KEY (`rota_id`) REFERENCES `rota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

-- Dumping data for table geoplaces.ponto: ~51 rows (approximately)
/*!40000 ALTER TABLE `ponto` DISABLE KEYS */;
INSERT INTO `ponto` (`id`, `lat`, `lng`, `icon`, `rota_id`, `order`, `historia`) VALUES
	(5, 39.0917759, -9.2600341, '', 19, 0, '<h2><span style="font-family:verdana,geneva,sans-serif">Sapo, Bordalo, Gato assanhado,&nbsp;</span></h2><h3 style="text-align:center"><span style="color:#008080"><span style="font-size:20px"><span style="font-family:verdana,geneva,sans-serif">Fabrica Rafael Bordalo Pinheiro</span></span></span></h3><h3><br></h3>'),
	(6, 38.7222524, -9.1393366, '', 19, 1, '<h1><em><span style="color: #ff6600;"><strong>Joao Maria&nbsp;</strong></span></em></h1>'),
	(25, 37.9261358, -8.770147, 'petrol', 19, 3, '<h1 style="text-align:center"><strong>Refinaria de Sines</strong></h1><p><br></p><h3 style="text-align:center"><u><span style="font-family:courier new,courier,monospace;">Blabla</span></u></h3>'),
	(33, 39.396159020294, -9.1278147697449, 'home', 20, 0, ''),
	(34, 38.835027691871, -9.1604948043823, 'shoppingcart', 20, 1, ''),
	(35, 38.492831622215, -8.4505462646484, 'restaurant', 20, 2, ''),
	(36, 37.062529546082, -8.0911087989807, 'beach', 20, 3, ''),
	(61, 38.64690824776, -8.9222717285156, '', 19, 2, '<h1>bla bla bla bla</h1>\n<h1>bla bla bla bla</h1>'),
	(63, 37.1028034, -8.6729253999999, 'beach', 19, 4, '<h1 style="text-align: center;"><strong>Selfies</strong></h1>'),
	(64, 37.0890719, -8.2478796, 'beach', 19, 5, '<p><br></p>'),
	(65, 37.0193548, -7.9304397, 'beach', 19, 6, '<h1 style="text-align: center;">Viagem a China</h1>'),
	(70, 39.879444540318, -8.2891845703125, 'home', 23, 0, ''),
	(71, 38.7067813, -9.1750291999999, 'sport', 23, 1, ''),
	(72, 38.3815245, -8.7855323, 'bar', 23, 2, ''),
	(74, 37.9261358, -8.770147, 'petrol', 23, 3, ''),
	(75, 37.1028034, -8.6729253999999, 'beach', 23, 4, ''),
	(76, 37.0890719, -8.2478796, 'beach', 23, 5, ''),
	(77, 37.0193548, -7.9304397, 'beach', 23, 6, ''),
	(78, 36.146746777814, -5.625, 'beach', 23, 7, ''),
	(81, 39.0917759, -9.2600341, 'bus', 24, 0, '<h2>Saida de casa as 10h</h2>'),
	(82, 38.7222524, -9.1393366, 'cafe', 24, 1, ''),
	(83, 38.520235228759, -8.96484375, 'ski', 24, 2, ''),
	(84, 39.750753494465, -8.2205200195312, 'home', 25, 0, ''),
	(85, 38.7067813, -9.1750291999999, 'sport', 25, 1, ''),
	(101, 41.1579438, -8.6291053, '', 31, 0, '<h1><strong>Aeroporto</strong></h1>'),
	(103, 42.2405989, -8.7207268, '', 31, 1, ''),
	(104, 38.706627903615, -9.1468048095703, '', 34, 0, ''),
	(106, 41.3850639, 2.1734034999999, 'home', 35, 0, '<h2 style="text-align:left"><span style="color:#FF8C00;"><strong>Saida de casa as 8h</strong></span></h2>'),
	(107, 48.856614, 2.3522219, 'airport', 35, 1, '<h2 style="text-align: left;"><span style="color:#A52A2A;">Chagada a Paris ás 14h</span></h2>'),
	(108, 37.422525934563, -5.965576171875, 'beach', 19, 7, '<p><br></p>'),
	(109, 39.094897129669, -9.2664957046509, '', 36, 0, '<p><br></p>'),
	(110, 39.08996756443, -9.260401725769, '', 36, 1, '<p><br></p>'),
	(111, 39.091916116198, -9.2539858818054, '', 36, 2, ''),
	(112, 39.087002997801, -9.2571616172791, '', 36, 3, ''),
	(113, 39.0917759, -9.2600341, '', 33, 0, '<h1>AAAAAAAAAAAAAAAAAAAAAAAA</h1>'),
	(117, 38.709826063617, -9.1462254524231, '', 34, 1, ''),
	(118, 38.711433461741, -9.1422772407532, '', 34, 2, ''),
	(119, 38.741632430146, -9.0982675552368, '', 34, 3, ''),
	(120, 38.761311802602, -9.0962505340576, '', 34, 4, ''),
	(121, 38.78085193143, -9.4976806640625, '', 34, 5, ''),
	(123, 36.721273880045, -4.39453125, 'barber', 19, 8, '<p>aasasas as as as</p>'),
	(124, 39.0917759, -9.2600341, '', 39, 0, ''),
	(128, 39.13589402932, -9.3812786755798, '', 39, 1, ''),
	(129, 38.7222524, -9.1393366, '', 33, 1, '<h1>BBBBBBBBBBBBBBBBBBBBB</h1>'),
	(130, 38.64690824776, -8.9222717285156, '', 33, 2, '<h1>CCCCCCCCCCCCCCCCCCCCCCCCC</h1>'),
	(133, 37.9261358, -8.770147, 'petrol', 33, 3, '<h1>TTTTTTTTTTTTTTTTTTTT</h1>'),
	(135, 37.1028034, -8.6729253999999, 'beach', 33, 4, '<h1>DDDDDDDDDDDDDDDDDDDDDD</h1>'),
	(138, 37.0890719, -8.2478796, 'beach', 33, 5, '<h1>EEEEEEEEEEEEEEEEEEEEE</h1>'),
	(139, 37.0193548, -7.9304397, 'beach', 33, 6, '<h1>XXXXXXXXXXXXXXXXXXXXXXX</h1>'),
	(140, 36.146746777814, -5.625, 'beach', 33, 7, '<h1>GGGGGGGGGGGGGGGGGGGGGG</h1>'),
	(141, 38.453588708942, -4.691162109375, 'casino', 33, 8, '<h1>JJJJJJJJJJJJJJJJJJJJJJJ</h1>'),
	(142, 38.7845614, -9.2158389, '', 41, 0, ''),
	(144, 39.393656463056, -9.1303682327271, '', 42, 0, '<h1 style="text-align: center;">fried eggs</h1>'),
	(145, 39.393623297905, -9.1288232803345, '', 42, 1, ''),
	(146, 38.737534929848, -9.1447449628906, '', 41, 1, ''),
	(147, 39.394754025138, -9.1294455528259, '', 42, 2, '');
/*!40000 ALTER TABLE `ponto` ENABLE KEYS */;


-- Dumping structure for table geoplaces.rota
CREATE TABLE IF NOT EXISTS `rota` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(250) DEFAULT NULL,
  `utilizador_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rota_utilizador` (`utilizador_id`),
  CONSTRAINT `FK_rota_utilizador` FOREIGN KEY (`utilizador_id`) REFERENCES `utilizador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- Dumping data for table geoplaces.rota: ~15 rows (approximately)
/*!40000 ALTER TABLE `rota` DISABLE KEYS */;
INSERT INTO `rota` (`id`, `descricao`, `utilizador_id`) VALUES
	(19, ' Caldas - S, Martinho - Foz do Arelho', 2),
	(20, 'Ferias em Faro', 2),
	(23, 'Xpto', 2),
	(24, 'Torres Lisboa', 2),
	(25, 'Lisboa Sines', 2),
	(31, 'Teste RotaXX', 2),
	(33, 'Viagem á India', 2),
	(34, 'bla bla bla', 2),
	(35, 'Barcelona Paris', 2),
	(36, 'Atalho', 2),
	(37, 'Ida a Santa Cruz', 2),
	(38, 'Ida  Santa Cruz', 2),
	(39, 'Ida a Santa Cruz2', 2),
	(40, 'Torres -> Casa Ferrari', 2),
	(41, 'Odivelas->IADE', 2),
	(42, 'teste XXXXX', 2);
/*!40000 ALTER TABLE `rota` ENABLE KEYS */;


-- Dumping structure for table geoplaces.utilizador
CREATE TABLE IF NOT EXISTS `utilizador` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `authenticationKey` varchar(100) DEFAULT NULL,
  `accessToken` varchar(100) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `tipoUtilizador` enum('Admin','Utilizador') NOT NULL DEFAULT 'Utilizador',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table geoplaces.utilizador: ~2 rows (approximately)
/*!40000 ALTER TABLE `utilizador` DISABLE KEYS */;
INSERT INTO `utilizador` (`id`, `nome`, `senha`, `authenticationKey`, `accessToken`, `email`, `telefone`, `tipoUtilizador`) VALUES
	(2, 'vitor', 'qwe123', 'ngggzGN5WQI9rDloVtXfvsLLG3MuvZDo', '', 'vitorcool@gmail.com', NULL, 'Utilizador'),
	(3, 'maria', 'qwe123', 'xxggzGN5WQI9rDloVtXfvsLLG3MuvZDo', '', 'mara@gggg.com', NULL, 'Utilizador');
/*!40000 ALTER TABLE `utilizador` ENABLE KEYS */;


-- Dumping structure for trigger geoplaces.ponto_before_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `ponto_before_delete` BEFORE DELETE ON `ponto` FOR EACH ROW BEGIN
update fotos set ponto_id=null where ponto_id=OLD.id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger geoplaces.utilizador_before_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `utilizador_before_delete` BEFORE DELETE ON `utilizador` FOR EACH ROW BEGIN
update fotos set utilizador_id=(select min(id) from utilizador) where utilizador_id=OLD.id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
