<?php

namespace app\controllers;


class Rota2Controller extends RotaController
{

    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
        \Yii::$app->user->enableAutoLogin = false;
        \Yii::$app->request->enableCookieValidation=true;
        header('Access-Control-Allow-Origin: *');
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['corsFilter']);
        unset($behaviors['access']);
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
            'except'=>['exception','get','login'],
        ];
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'except'=>['exception','get','login'],
            'rules' => [
                // allow authenticated users
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ]
        ];
        return $behaviors;
    }


}
