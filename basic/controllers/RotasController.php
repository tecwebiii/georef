<?php

namespace app\controllers;

use app\models\Fotos;
use Yii;
use app\models\Rota;
use app\models\RotaSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RotasController implements the CRUD actions for Rota model.
 */
class RotasController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except'=>['ver','index'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ]
                    // everything else is denied
                ],
            ],

        ];
    }


    /**
     * Lists all Rota models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RotaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionNova(){
        return $this->addEdit();
    }

    public function actionAltera(){
        return $this->addEdit();
    }


    private function addEdit()
    {
        $id=Yii::$app->request->get('id');
        if($this->action->id=='nova' && !empty($id))
            return $this->redirect(array_merge(['rotas/altera'],Yii::$app->request->get()));
        if($this->action->id=='altera' && empty($id))
            return $this->redirect('nova');

        $this->css=[
            "/css/jquery.fileupload.css",
            "/css/jquery.fileupload-ui.css",
            "/css/ng-ckeditor.css",
        ];

        $this->javaScript=[
            "/js/html2canvas.js",
            "/js/vendor/jquery.ui.widget.js",
            "//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js",
            "//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js",
            "//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js",
            "//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js",
            "//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js",
            "/js/jquery.iframe-transport.js",
            "/js/jquery.fileupload.js",
            "/js/jquery.fileupload-process.js",
            "/js/jquery.fileupload-image.js",
            "/js/jquery.fileupload-video.js",
            "/js/jquery.fileupload-validate.js",
            "/js/jquery.fileupload-angular.js",
            "/js/ckeditor/ckeditor.js",
            "/js/ng-ckeditor.js",
            "/js/routemap.js",
            "/js/leftPanel.js",
            "/js/resizeMap.js",
            "/js/app.js",
            ];


        $foto = new Fotos();
        return $this->render('rota',['foto'=>$foto]);
    }

    /**
     * Displays a single Rota model.
     * @param string $id
     * @return mixed
     */
    public function actionVer($id)
    {
        $id=Yii::$app->request->get('id');
        if($this->action->id=='nova' && !empty($id))
            return $this->redirect(array_merge(['rotas/altera'],Yii::$app->request->get()));
        if($this->action->id=='altera' && empty($id))
            return $this->redirect('nova');

        $this->javaScript=[
            "/js/flexslider/jquery.flexslider.js",
    //        "/js/ng-flexslider/angular-flexslider.js",
//            "/js/jssor.slider.mini.js",
            "/js/routemap.js",
            "/js/resizeMap.js",
            "/js/app2.js",
        ];

        $this->css=["/js/flexslider/flexslider.css"];

        $foto = new Fotos();
        return $this->render('view',['foto'=>$foto]);
    }


    /**
     * @return mixed
     */
    public function actionTest()
    {
        $this->javaScript=[
            "/js/ng-ckeditor.js",
            "/js/verrota.js",
            "/js/leftPanel.js",
            "/js/resizeMap.js"
        ];

        $this->css=[
        ];
        return $this->render('test');

    }

    public function beforeAction($event)
    {
        return parent::beforeAction($event);
    }

    /**
     * Finds the Rota model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Rota the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rota::find()->joinWith(['pontos','pontos.fotos'])->where(['rota.id'=>$id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
