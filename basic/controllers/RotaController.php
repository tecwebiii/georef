<?php

namespace app\controllers;

use app\models\Utilizador;
use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\filters\VerbFilter;
use app\models\Rota;
use app\models\Ponto;
use app\models\Fotos;
use yii\helpers\Url;
use yii\web\UploadedFile;
use app\models\LoginForm;

require(\Yii::getAlias('@webroot').'/upload/UploadHandler.php');


class RotaController extends Controller
{

    private function reconstruct_url($url){
        $url_parts = parse_url($url);
        $constructed_url = $url_parts['scheme'] . '://' . $url_parts['host'] . (isset($url_parts['path'])?$url_parts['path']:'');

        return $constructed_url;
    }

    public function behaviors()
    {
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];

        $behaviors['corsFilter'] = [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers' => ["Origin"," X-Requested-With","Content-Type", "Accept","Content-Range", "Content-Disposition", "Content-Description"],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Allow-Origin' => ["*"],//'+$this->reconstruct_url(_SERVER['REQUEST_URI'])+'",
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 1728000,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
            ];
        $behaviors['cors'] = [
            'class' => \yii\filters\Cors::className(),

            #common rules
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST','GET'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ]
        ];
        $behaviors['verbs'] = [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ];

        $behaviors['access'] = [
                'class' => \yii\filters\AccessControl::className(),
                'except'=>['exception','get','login'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                        'denyCallback'=>function($rule,Action $action){
                            $data=['action'=>$action->id,
                                   'error'=>203,
                                   'message'=>Yii::t('app',
                                        'Access denied. The action "{action}" needs authentication.',
                                       ['action'=>$action->id])
                                    ];
                            //return renderPartial('_ajax',['data'=>$data]);
                            $response = Yii::$app->response;
                            $response->statusCode=203;
                            $response->format = \yii\web\Response::FORMAT_JSON;
                            $response->data = $data;

                            Yii::$app->end(203,$response);
                        }
                    ]
                    // everything else is denied
                ],
            ];

        return $behaviors;
    }

    public function actionLogin()
    {
        $data=['action'=>'login'];

        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            $data['data']=$model->getUser()->toArray(['id','nome','email','telefone','tipoUtilizador']);
            $data['access_token'] = Yii::$app->user->identity->getAuthKey();
            $data['message'] = !empty($data['access_token']) ? 'ok' : 'invalid token';
        } else {
            $model->validate();
            $data['message']=$model->getErrors();
        }
        return $data;

        /*
        $data=['action'=>'login'];
        $this->enableCsrfValidation = false;
        $model = new LoginForm();

        $post = ['LoginForm'=>[
                        'username'=> Yii::$app->request->post('nome'),
                        'password'=> Yii::$app->request->post('senha')
            ]];

        if (Yii::$app->request->isPost && $model->load($post) &&  $model->login()) {
            $data['message']= 'ok';
            $data['data'] = $model->getUser()->toArray(['id','nome','email','telefone','tipoUtilizador']);
         }else {
            $data['data'] = $post;
            $data['message'] = Yii::t('app', 'Utilizador ou senha incorrecta.');
        }

        return $this->renderPartial('_ajax', ['data' => $data]);*/
    }

    public function actionAddupdate()
    {
        $data=['action'=>'add_update'];

        $id = intval(Yii::$app->request->post('id'));
        if(!empty($id))
            $model = Rota::find()->where([
                            'id'=>$id,
                            'utilizador_id'=>Yii::$app->user->identity->getId()])->one();
        else
            $model = new Rota();

        $post = Yii::$app->request->post();
        $post['utilizador_id']=Yii::$app->user->identity->getId();
        unset($post['id']);

        if (!empty($model) && $model->load(['Rota'=>$post]) && $model->validate()) {
            $model->save();
            $data['message']='ok';
            $params=(object) ['id'=>$model->id,'data'=>$data];
            $this->getRota($params);
            $data= $params->data;
            //$data['data'] = ['Rota'=>$model->toArray()];
        } else {
            $data['error']=400;
            $data['message']=$model->errors;
        }

        return $data;
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }

    public function actionDelete()
    {
        $data=['action'=>'delete'];

        $data=['action'=>'get'];
        $id=Yii::$app->request->get('id');
        $model= Rota::find()
            ->where(['id'=>$id])
            ->andWhere(['rota.utilizador_id'=>Yii::$app->user->identity->getId()])
            ->one();
        if(empty($model)){
            $data['error']=400;
            $data['message']=Yii::t('app','Route ID is missing');
        }else {
            if($model->delete()==0) {
                $data['error']=400;
                $data['message']=Yii::t('app','Can not delete route ID {id}',['id'=>$id]);
            }else{
                $data['message']='ok';
            }
        }
        return $data;
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }

    public function actionDeletepoint()
    {
        $data=['action'=>'deletepoint'];

        $data=['action'=>'get'];
        $id=Yii::$app->request->get('id');
        $order=Yii::$app->request->get('order');
        $model= Ponto::find()
            ->joinWith(['rota'])
            ->where(['ponto.rota_id'=>$id])
            ->andWhere(['ponto.order'=>$order])
            ->andWhere(['rota.utilizador_id'=>Yii::$app->user->identity->getId()])
            ->one();
        if(empty($model)){
            $data['error']=400;
            $data['message']=Yii::t('app','Point order {order} from route ID {id} is missing',['order'=>order,'id'=>id]);
        }else {
            $transaction =  Yii::$app->db->beginTransaction();
            try {
                if($model->delete()==0) {
                    $data['error']=400;
                    $data['message']=Yii::t('app','Can not delete order {order} from route ID {id}', ['order'=>order,'id'=>id]);
                    $transaction->rollBack();
                }else{
                    $data['message']='ok';
                    $model=Ponto::find()
                        ->joinWith(['rota'])
                        ->where(['ponto.rota_id'=>$id])
                        ->andWhere(['>','ponto.order',$order])
                        ->andWhere(['rota.utilizador_id'=>Yii::$app->user->identity->getId()])
                        ->all();
                    foreach($model as $ponto){
                        $ponto->order=$ponto->order-1;
                        $ponto->save();
                    }
                    $transaction->commit();
                }
            }catch(\Exception $e){
                $data['error'] = 400;
                $data['message'] = $e->getMessage();
                $transaction->rollBack();
            }
        }
        return $data;
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }


    private function getRota($params){
        $model= Rota::find()->where(['id'=>$params->id])
                            ->one();
        if(empty($model)){
            $params->data['error']=400;
            $params->data['message']=Yii::t('app','Route ID is missing');
        }else {
            $params->data['message']='ok';
            $params->data['data'] = ['Rota'=>$model->toArray(),
                'Pontos'=>$model->getPontos()->orderBy(['order'=>SORT_ASC])->all()];
            foreach($params->data['data']['Pontos'] as $i=>$ponto){
                $params->data['data']['Pontos'][$i] = $ponto->toArray();
                $params->data['data']['Pontos'][$i]['Fotos'] = $ponto->getFotos()->all();
                foreach($params->data['data']['Pontos'][$i]['Fotos'] as $f=>$foto){
                    $params->data['data']['Pontos'][$i]['Fotos'][$f] = $foto->allFields();
                }
            }
        }

    }

    public function actionGet()
    {
        $data=['action'=>'get'];
        $id=Yii::$app->request->get('id');

        $params=(object) ['id'=>$id,'data'=>$data];
        $this->getRota($params);
        return $params->data;
        /*$model= Rota::find()->where(['id'=>$id])
            ->one();
        if(empty($model)){
            $data['error']=400;
            $data['message']=Yii::t('app','Route ID is missing');
        }else {
            $data['message']='ok';
            $data['data'] = ['Rota'=>$model->toArray(),
                             'Pontos'=>$model->getPontos()->orderBy(['order'=>SORT_ASC])->all()];
            foreach($data['data']['Pontos'] as $i=>$ponto){
                $data['data']['Pontos'][$i] = $ponto->toArray();
                $data['data']['Pontos'][$i]['Fotos'] = $ponto->getFotos()->all();
                foreach($data['data']['Pontos'][$i]['Fotos'] as $f=>$foto){
                    $data['data']['Pontos'][$i]['Fotos'][$f] = $foto->allFields();
                }
            }
        }

        return $data;*/
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }

    public function actionList()
    {
        $data=['action'=>'list'];
        $model = Rota::find()
            ->joinWith(['utilizador'])
            ->select(['rota.id',
                        'rota.descricao',
                        'rota.utilizador_id',
                        ])
            ->where(['rota.utilizador_id'=>Yii::$app->user->identity->getId()]);
        $rotas=$model->all();
        $data['data']=$model->asArray()->all();
        $data['message']='ok';
        foreach($rotas as $i=>$rota){
            $a=$rota->getSelectedFoto()->one();
            $data['data'][$i]['foto']=empty($a)?[]:$a->allFields();;
        }
        return $data;
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }

    public function actionUpdate()
    {
        $data=['action'=>'update'];

        $post = Yii::$app->request->post();
        if(! isset($post['Rota'])){
            $data['error']=400;
            $data['message']=Yii::t('app','Route data is missing');
        }else{
            $rota = $post['Rota'];
            $pontos = $post['Pontos'];
            $model= Rota::find()->joinWith(['pontos','pontos.fotos'],true,'LEFT JOIN')
                            ->where(['rota.id'=>$rota['id']])
                            ->andWhere(['rota.utilizador_id'=>Yii::$app->user->identity->getId()])
                            ->orderBy('ponto.order')->one();
            if(empty($model)){
                $data['error']=400;
                $data['message']=Yii::t('app','Updating route id is missing');
            }else{

                if ($model->load(['Rota'=>$rota]) && $model->validate()) {
                    $transaction =  Yii::$app->db->beginTransaction();
                    try {
                        $model->save();
                        foreach ($pontos as $i => $ponto) {
                            $model2 = $model->getPontos()->where(['ponto.order' => $i])->one();
                            if(empty($model2)) {
                                $model2 = new Ponto();
                            }
                            $ponto['rota_id'] = $rota['id'];
                            $ponto['order']   = $i;
                            if ($model2->load(['Ponto'=>$ponto]) && $model2->validate()) {
                                $model2->save();
                            }else{
                                $data['error'] = 400;
                                $data['message'] =$model2->errors;
                                $transaction->rollBack();
                            };
                        }
                        if(empty($data['message'])) {
                            $model3= $model->getPontos()
                                ->andWhere(['>=','order',sizeof($pontos)])
                                ->all();
                            foreach($model3 as $toDelete) {
                                $toDelete->delete();
                            }
                            $data['message'] = 'ok';
                            $data['data'] = ['Rota' => $model->toArray()];
                            $transaction->commit();
                        }
                    }catch(\Exception $e){
                        $data['error'] = 400;
                        $data['message'] = $e->getMessage();
                        $transaction->rollBack();
                    }
                } else {
                    $data['error'] = 400;
                    $data['message'] = $model->errors;
                }

            }
        }

        return $data;
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        //header("Content-type: application/json; charset=utf-8");
        return parent::beforeAction($action);
    }

    public function actionPath(){
        $model=new Fotos();
        $files=[
            $model->getFullPath('teste.jpg'),
            $model->getThumbnailFullPath('teste.jpg')
        ];
        return $files;

    }

    public function actionDeletefoto(){
        $data=['action'=>'deletefoto'];
        if (Yii::$app->request->isPost) {
            $fich = Yii::$app->request->post('ficheiro');
            $ponto_id = Yii::$app->request->post('ponto_id');

            if(isset($fich) && isset($ponto_id)) {
                $model=Fotos::find()->where([
                                    'ficheiro'=>$fich,
                                    'ponto_id'=>$ponto_id])->one();
                if($model!=null && $model->utilizador_id=Yii::$app->user->identity->getId()){
                    $files=[
                            $model->getFullPath($fich),
                            $model->getThumbnailFullPath($fich)
                           ];
                    if($model->delete())
                        $this->deleteFiles($files);
                    $data['message'] = 'ok';
                }else{
                    $data['error'] = 400;
                    $data['message'] = Yii::t('app','Can not delete file.');
                }

            }else{
                $data['error'] = 400;
                $data['message'] = Yii::t('app','Invalid data');
            }
        }else{
            $data['error'] = 405;
            $data['message'] = Yii::t('app','Method Not Allowed');
        }
        return $data;
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }

    private function deleteFiles( $files ){
        foreach($files as $file)
            unlink($file);
    }

    public function actionUpdatehistoria()
    {
        $data=['action'=>'updatehistoria'];
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if(isset($post['id']) && isset($post['historia'])) {
                $model = Ponto::find()->where(['id'=>$post['id']])->one();

                if ($model!=null){
                    $model->historia=$post['historia'];
                    $model->save();
                    // file is uploaded successfully
                    $data['message'] = 'ok';
                    $data['data'] = $model->toArray();
                }
            }else{
                $data['error'] = 400;
                $data['message'] = Yii::t('app','Invalid data');
            }
        }else{
            $data['error'] = 405;
            $data['message'] = Yii::t('app','Method Not Allowed');
        }

        return $data;
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }

    public function actionInsertfotos()
    {
        $data=['action'=>'insertfotos'];
        if (Yii::$app->request->isPost) {
            $fotos = Yii::$app->request->post();

            if(isset($fotos['fotos'])) {
                foreach ($fotos['fotos'] as $foto) {
                    $foto['utilizador_id'] = Yii::$app->user->identity->getId();
                    $model = new Fotos();
                    if ($model->load(['Fotos'=>$foto]) && $model->validate()) { //$model->upload()) {
                        $model->save();
                        // file is uploaded successfully
                        $data['message'] = 'ok';
                        $data['data'] = $model->toArray();
                    } else {
                        $data['error'] = 400;
                        $data['message'] = $model->errors;
                    }
                }
            }else{
                $data['error'] = 400;
                $data['message'] = Yii::t('app','Invalid data');
            }
        }else{
            $data['error'] = 405;
            $data['message'] = Yii::t('app','Method Not Allowed');
        }

        return $data;
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }

    public function actionUploadmapfoto()
    {
        $data=['action'=>'insertfotos'];
        if (Yii::$app->request->isPost) {
            $img = $_POST['imgBase64'];
            $file = $_POST['id'];
            $file = "upload/routes/route_{$file}.png";
            $uri =  substr($img,strpos($img,",")+1);
            file_put_contents($file, base64_decode($uri));
            $data['file'] = $file;

        }else{
            $data['error'] = 405;
            $data['message'] = Yii::t('app','Method Not Allowed');
        }

        return $data;
        //return $this->renderPartial('_ajax',['data'=>$data]);
    }

}
