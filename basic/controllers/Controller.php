<?php
/**
 * Created by PhpStorm.
 * User: vao
 * Date: 22/03/2016
 * Time: 22:00
 */

namespace app\controllers;

use yii\base\Action;

class Controller extends \yii\web\Controller
{
    public $javaScript="";
    public $css="";

    public function beforeAction($action)
    {
        $this->getView()->title = $action->getUniqueId();
        return parent::beforeAction($action);
    }
}