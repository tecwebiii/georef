


angular.module('app.routes', [])

    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

      // Ionic uses AngularUI Router which uses the concept of states
      // Learn more here: https://github.com/angular-ui/ui-router
      // Set up the various states which the app can be in.
      // Each state's controller can be found in controllers.js
      $stateProvider

      .state('menu.inicio/:id', {
        url: '/inicio/:id',
        views: {
          'side-menu21': {
            templateUrl: 'templates/inicio.html',
            controller: 'inicioCtrl'
          }
        }
      })

      .state('menu.rotas', {
        url: '/rotas',
        views: {
          'side-menu21': {
            templateUrl: 'templates/rotas.html',
            controller: 'rotasCtrl'
          }
        }
      })


      .state('menu', {
        url: '/side-menu',
        templateUrl: 'templates/menu.html',
        controller: 'appCtrl',
        abstract:true

      })

      .state('menu.entrar', {
        url: '/Entrar',
        views: {
          'side-menu21': {
            templateUrl: 'templates/entrar.html',
            controller: 'entrarCtrl'
          }
        }
      })

      .state('menu.subscriO', {
        url: '/subscrever',
        views: {
          'side-menu21': {
            templateUrl: 'templates/subscriO.html',
            controller: 'subscriOCtrl'
          }
        }
      })

      .state('menu.opEs', {
            url: '/opcoes',
            views: {
              'side-menu21': {
                templateUrl: 'templates/opEs.html',
                controller: 'opEsCtrl'
              }
            }
          })

      .state('menu.teste', {
            url: '/teste',
            views: {
              'side-menu21': {
                templateUrl: 'templates/teste.html',
                controller: 'MapMarkerFileUploadController'
              }
            }
          })
      .state('menu.pontos/:id', {
        url: '/pontos/:id',
        views: {
          'side-menu21': {
            templateUrl: 'templates/pontos.html',
            controller: 'pontosController'
          }
        }
      })


    $httpProvider.interceptors.push('authInterceptor');
    $urlRouterProvider.otherwise('/side-menu/inicio/0');



    });