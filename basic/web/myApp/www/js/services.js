


angular.module('app.services', [])
    .factory('authInterceptor', function ($q, $window, $location) {
        return {
            request: function (config) {
                if ($window.sessionStorage.access_token) {
                    //HttpBearerAuth
                    config.headers.Authorization = 'Bearer ' + $window.sessionStorage.access_token;
                }
                return config;
            },
            responseError: function (rejection) {
                if (rejection.status === 401) {
                    delete $window.sessionStorage.access_token;
                    delete $window.sessionStorage.user;
                    $location.path('/side-menu/Entrar').replace();
                }
                return $q.reject(rejection);
            }
        };
    })
    .factory('BlankFactory', [function(){
    }])
    .service('JSONService', ['$http',function($http){
        var myAlert=function(message) {
            $ionicPopup.alert({
                title: 'Message',  // String. The title of the popup.
                template: message, // String (optional). The html template to place in the popup body.
                okText: 'Fechar', // String (default: 'OK'). The text of the OK button.

            });
        }
        this.post=function( url , data, callback, callbackError ) {

            $http({url:url,method:'post',data:data,dataType: 'jsonp',
                headers: { 'Content-Type': 'application/json;charset=utf-8' }})
                .success(function (data, p2) {
                    if (data['message'] != 'ok') {
                        if (typeof(data['message']) == 'string')
                            data['message'] = [data['message']];
                        var m = '';
                        jQuery.each(data['message'], function (i, mess) {
                            m = (m == '' ? m : '<br>' + m) + mess;
                        });
                        if (typeof(callbackError) == "function")
                            callbackError(m);
                        else
                            myAlert(m);
                    } else {
                        if (typeof(callback) == "function")
                            callback(data);
                    }
                })
                .error(function (data, error) {
                    if (data!=null && typeof(data.message) != 'undefined')
                        if (typeof(callbackError) == "function")
                            callbackError(data.message);
                        else
                            myAlert(data.message);
                });

        }

        this.get=function( url , callback, callbackError ) {

            $http({url:url,method:'get',dataType: 'jsonp',
                headers: { 'Content-Type': 'application/json;charset=utf-8' }})
                .success(function (data, p2) {
                    if (data['message'] != 'ok') {
                        if (typeof(data['message']) == 'string')
                            data['message'] = [data['message']];
                        var m = '';
                        jQuery.each(data['message'], function (i, mess) {
                            m = (m == '' ? m : '<br>' + m) + mess;
                        });
                        if (typeof(callbackError) == "function")
                            callbackError(m);
                        else
                            myAlert(m);
                    } else {
                        if (typeof(callback) == "function")
                            callback(data);
                    }
                })
                .error(function (data, error) {
                    if (data!=null && typeof(data.message) != 'undefined')
                        if (typeof(callbackError) == "function")
                            callbackError(data.message);
                        else
                            myAlert(data.message);
                });

        }
    }])
    .service('BlankService', [function(){
    }])
    .service('gooIconsService', [function(){
        this.icons = [
            {icon: 'academy', name: 'Academia'},
            {icon: 'activities', name: 'Actividades'},
            {icon: 'airport', name: 'Aeroporto'},
            {icon: 'amusement', name: 'Divertimento'},
            {icon: 'aquarium', name: 'Aquario'},
            {icon: 'art-gallery', name: 'Galeria de Arte'},
            {icon: 'baby', name: 'Bebe'},
            {icon: 'bank-dollar', name: 'Dollar'},
            {icon: 'bank-euro', name: 'Euro'},
            {icon: 'bank-pound', name: 'Libra'},
            {icon: 'bar', name: 'Bar'},
            {icon: 'barber', name: 'Barbeiro'},
            {icon: 'beach', name: 'Praia'},
            {icon: 'beer', name: 'Cerveja'},
            {icon: 'bicycle', name: 'Bicicleta'},
            {icon: 'books', name: 'Livros'},
            {icon: 'bowling', name: 'Bowling'},
            {icon: 'bus', name: 'Autocarro'},
            {icon: 'cafe', name: 'Café'},
            {icon: 'camping', name: 'Campismo'},
            {icon: 'car-dealer', name: 'Venda de carros'},
            {icon: 'car-rental', name: 'Aluguer de carros'},
            {icon: 'car-repair', name: 'Reparação de carros'},
            {icon: 'casino', name: 'Casino'},
            {icon: 'caution', name: 'Cuidado'},
            {icon: 'cemetery-grave', name: 'Cemiterio'},
            {icon: 'cinema', name: 'Cinema'},
            {icon: 'civic-building', name: 'Construção civil'},
            {icon: 'computer', name: 'Computador'},
            {icon: 'corporate', name: 'Corporativo'},
            {icon: 'courthouse', name: 'Tribunal'},
            {icon: 'fire', name: 'Fogo'},
            {icon: 'flag', name: 'Bandeira'},
            {icon: 'floral', name: 'Florar'},
            {icon: 'helicopter', name: 'Helicoptero'},
            {icon: 'home', name: 'Casa'},
            {icon: 'info', name: 'Informação'},
            {icon: 'landslide', name: 'Desmoronamento'},
            {icon: 'locomotive', name: 'Locomotiva'},
            {icon: 'medical', name: 'Medico'},
            {icon: 'mobile', name: 'Movel'},
            {icon: 'motorcycle', name: 'Motociclo'},
            {icon: 'music', name: 'Musica'},
            {icon: 'parking', name: 'Parque Automovel'},
            {icon: 'pet', name: 'Animal de Estimação'},
            {icon: 'petrol', name: 'Combustivel'},
            {icon: 'phone', name: 'Telefone'},
            {icon: 'picnic', name: 'Picnic'},
            {icon: 'postal', name: 'Correio'},
            {icon: 'repair', name: 'Reparação'},
            {icon: 'restaurant', name: 'Restaurante'},
            {icon: 'sail', name: 'Navegar'},
            {icon: 'school', name: 'Escola'},
            {icon: 'ship', name: 'Navio'},
            {icon: 'shoppingcart', name: 'Compras'},
            {icon: 'ski', name: 'Esqui'},
            {icon: 'snow', name: 'Neve'},
            {icon: 'sport', name: 'Desporto'},
            {icon: 'star', name: 'Estrela'},
            {icon: 'swim', name: 'Nadar'},
            {icon: 'taxi', name: 'Taxi'},
            {icon: 'train', name: 'Comboio'},
            {icon: 'truck', name: 'Camião'},
            {icon: 'wc-female', name: 'WC-Femenino'},
            {icon: 'wc-male', name: 'WC-Masculino'},
            {icon: 'wc', name: 'WC-Casa de banho'},
            {icon: 'wheelchair', name: 'Cadeira de rodas'}
        ];
        this.icons.sort(function(a, b){
            return (a.name > b.name) ? 1 : (a.name< b.name) ? -1 : 0;
        });

        this.getName=function(icon_name) {
            var icon = this.icons.filter(function(i){
                return i.icon==icon_name;
            });
            return (icon.length==0?'':icon[0].name)
        }

        var fontSize=[10,12,16];
        var iconSize=[12,16,24];
        this.getUrl= function( icon_name, size_1_2_3 , showText){
            showText = typeof(showText)=='undefined'?true:false;
            //d_simple_text_icon_left
            //chst=<icon_position_string>
            //chld=<text>|<font_size>|<font_fill_color>|<icon_name>|<icon_size>|<icon_fill_color>|<icon_and_text_border_color>

            size_1_2_3=(size_1_2_3!=undefined && size_1_2_3>=1 && size_1_2_3<=3)?size_1_2_3:3;
            var name = this.getName(icon_name);

            return "http://chart.apis.google.com/chart?"+
                "chst="+
                "d_simple_text_icon_left&"+
                "chld=" + (showText?(name==''?'caution':name):'') + "|"+  //text
                fontSize[size_1_2_3-1]+"|"+  //font_size
                "000000"+"|"+                //font_fill_color
                icon_name+"|"+                      //icon_name
                iconSize[size_1_2_3-1]+"|"+         //icon_size [12,16,24]
                "000000"+"|"+                       //icon_fill_color
                "FFFFFF";                           //icon_and_text_border
        }
    }])

    .service('routeListService',['$rootScope','routeService','$http','$ionicPopup','$timeout',
        function($rootScope,routeService,$http,$ionicPopup,$timeout){
            var myAlert=function(message) {
                $ionicPopup.alert({
                    title: 'Message',  // String. The title of the popup.
                    template: message, // String (optional). The html template to place in the popup body.
                    okText: 'Fechar', // String (default: 'OK'). The text of the OK button.

                });
            }
            /**
             * * load authenticated user route list
             * @param callback
             */
            this.load=function(callback){
                $http.get(window.getHost()+"/rota2/list")
                    .success(function (data,p2) {
                        if (data['message'] != 'ok') {
                            if (typeof(data['message']) == 'string')
                                data['message'] = [data['message']];
                            var m='';
                            jQuery.each(data['message'], function (i, mess) {
                                m=(m==''?m:'<br>'+m)+mess;
                            });

                            myAlert(m);
                        } else {
                            if (typeof(callback) == "function")
                                var ret=callback(data.data);
                        }
                    })
                    .error(function (data, error) {
                        if(typeof(data.message)!='undefined')
                            myAlert(data.message);
                    });
            }

        }
    ])

    .service('mapService',['$rootScope','routeService','$timeout','gooIconsService','$ionicPopup',

        function($rootScope,routeService,$timeout,gooIconsService,$ionicPopup) {
            var me = this;

            var mapObj = function () {
                return jQuery("#map");
                ;
            }

            this.map = function () {
                var ret = mapObj().data('map');
                if (ret == undefined) {
                    console.log("Gmaps object created.")
                    ret = createMap(mapObj());
                }
                return ret;
            }

            this.releaseMap=function(removeMap){
                removeMap=removeMap==undefined?true:false;
                var map= mapObj().data("map");
                mapObj().removeData("map");
                if(removeMap)
                    mapObj().remove();
                else
                    mapObj().html('');
                delete map;
                console.log("#map removed.");
            }

            this.updatePonto=function(ponto){
                if(ponto==undefined || ponto.order==undefined) return {};
                var idx=parseInt(ponto.order);
                if(typeof(me.getRouteList())=="object" &&
                    typeof(me.getRouteList().length)!="undefined" &&
                    me.getRouteList().length>idx){
                    ponto.address=me.getRouteList()[idx].address;
                    ponto.lat=me.getRouteList()[idx].lat;
                    ponto.lng=me.getRouteList()[idx].lng;
                }
                return ponto;
            }

            this.icon = [];
            var maxPins = 10;

            var createMap = function (divObj) {
                if(divObj.length==0)
                    return undefined;

                var ret = null;
                jQuery(window).resize(function (e) {
                    divObj.height(jQuery(window).height() - divObj.offset().top - 13);
                    if(ret!=null)
                        ret.refresh();
                });
                $(window).trigger('resize');

                ret = new GMaps({
                    div: divObj[0],
                    lat: 39.391714,
                    lng: -9.130700,
                    zoom: 16,
                    styles: [{
                        "elementType": "geometry",
                        "stylers": [{"hue": "#ff4400"}, {"saturation": -68}, {"lightness": -4}, {"gamma": 0.72}]
                    }, {"featureType": "road", "elementType": "labels.icon"}, {
                        "featureType": "landscape.man_made",
                        "elementType": "geometry",
                        "stylers": [{"hue": "#0077ff"}, {"gamma": 3.1}]
                    }, {
                        "featureType": "water",
                        "stylers": [{"hue": "#00ccff"}, {"gamma": 0.44}, {"saturation": -33}]
                    }, {
                        "featureType": "poi.park",
                        "stylers": [{"hue": "#44ff00"}, {"saturation": -23}]
                    }, {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [{"hue": "#007fff"}, {"gamma": 0.77}, {"saturation": 65}, {"lightness": 99}]
                    }, {
                        "featureType": "water",
                        "elementType": "labels.text.stroke",
                        "stylers": [{"gamma": 0.11}, {"weight": 5.6}, {"saturation": 99}, {"hue": "#0091ff"}, {"lightness": -86}]
                    }, {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [{"lightness": -48}, {"hue": "#ff5e00"}, {"gamma": 1.2}, {"saturation": -23}]
                    }, {
                        "featureType": "transit",
                        "elementType": "labels.text.stroke",
                        "stylers": [{"saturation": -64}, {"hue": "#ff9100"}, {"lightness": 16}, {"gamma": 0.47}, {"weight": 2.7}]
                    }],
                    click: function (e) {
                        google.maps.event.trigger(me.map().map, 'rightclick', e);
                    },
                    zoom_changed: function (e) {
                        jQuery("#zoomLevel").val(me.zoom());
                        jQuery("#ZoomLabel").html('Zoom: ' + me.zoom());
                    },
                    idle: function (e) {
                        jQuery("#zoomLevel").val(me.zoom());
                        jQuery("#ZoomLabel").html('Zoom: ' + me.zoom());
                    }

                });
                divObj.data('map', ret);

                return ret;
            };

            var setupContextMenus=function(){
                me.map().setContextMenu({
                    control: 'map',
                    options: $.merge(
                        (me.isEditable() ? [{
                            title: 'Adicionar ponto',
                            name: 'add_marker',
                            action: function (e) {
                                me.addMarker(e.latLng.lat(), e.latLng.lng(),true);
                                me.drawRoute();
                            }
                        }] : []),
                        [centerHereOption,
                            centerOnMyLocation,
                            centerRoute,
                            zoomOption,
                            findPlaceOption])
                });
                me.map().setContextMenu({
                    control: 'marker',
                    options: $.merge($.merge(
                            (me.isEditable() ? [{
                                title: 'Apagar Ponto',
                                name: 'apagar_ponto',
                                action: function (e) {
                                    me.deleteMarker(e.marker);
                                }
                            }] : []),
                            [   centerHereOption,
                                centerRoute,
                                zoomOption,
                                findPlaceOption]),
                        (me.isEditable() ? [selectIconOption] : [])
                    )
                });
            }

            this.updateMarkers = function () {
                var backColor = new Rainbow();
                backColor.setNumberRange(1, Math.max(2, this.map().markers.length));
                backColor.setSpectrum('#006600', '#cccc00');
                var fontColor = new Rainbow();
                fontColor.setNumberRange(1, Math.max(2, this.map().markers.length));
                fontColor.setSpectrum('white', 'white');

                var i = 0;
                for (i = 0; i < this.map().markers.length; i++) {
                    texto = (i+1)+"";
                    icon = this.icon[i];
                    var url = (icon=='' || icon==null)
                        ? "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + texto + "|" + backColor.colourAt(i) + "|" + fontColor.colourAt(i)
                        : "http://chart.apis.google.com/chart?chst=d_bubble_icon_text_small&chld="+icon+"|edge_bc|"+texto+ " |" + backColor.colourAt(i) + "|" + fontColor.colourAt(i);
                    this.map().markers[i].setIcon(url);
                    var title = "Ponto: " + (i + 1);
                    title+=((icon=='' || icon==null)?''
                        :', '+gooIconsService.getName(icon));
                    this.map().markers[i].setTitle(title);
                }
            }



            this.clearRoute = function () {
                me.map().cleanRoute();
                me.map().removeMarkers();
                me.icon= [];
            }

            this.isEditable=function(){
                return routeService.isEditable();
            }
            var draw_point_element=function( order , address ){
                var ret='<div class="row ponto_historia" data-order="'+order+'">';
                ret += '  <div class="col-xs-1">'+(order+1)+'</div>';
                ret += '  <div class="col-xs-9">'+address+'</div>';
                ret += '  <div class="col-xs-2"><button class="add_foto_button btn-primary">foto</button></div>';

                ret+="</div>";
                return ret;
            }
            //jQuery(document).on('click','button.add_foto_button',event_handle_add_foto_button);

            var routeList=[];
            this.getRouteList=function(){
                if(routeList.length< me.map().markers.length)
                    for(var i=routeList.length; i<me.map().markers.length;i++){
                        var pos = me.map().markers[i].getPosition();
                        routeList.push({
                            address:me.map().markers[i].address!=undefined
                                ? me.map().markers[i].address
                                : 'Lat:'+pos.lat()+' Lng:'+pos.lng(),
                            lat:pos.lat(),
                            lng:pos.lng(),
                            icon:me.icon[i]
                        });
                    }

                return routeList;
            }

            this.drawRoute = function (callback) {

                if(me.map().markers.length==0) {
                    routeList=[];
                    if (typeof(callback) == "function")
                        callback(routeList);
                    return;
                }

                me.updateMarkers();

                var origin = me.map().markers[0].getPosition();
                var destination = me.map().markers[me.map().markers.length - 1].getPosition();
                var i = 0, waypoints = [];
                for (i = 1; i < me.map().markers.length - 1; i++) {
                    waypoints.push({
                        location: me.map().markers[i].getPosition().lat() + ", " + me.map().markers[i].getPosition().lng(),
                        stopover: true
                    });
                }

                me.map().cleanRoute();
                me.map().drawRoute({
                    origin: [origin.lat(), origin.lng()],
                    destination: [destination.lat(), destination.lng()],
                    waypoints: waypoints,
                    optimizeWaypoints: false,//waypoints.length != 0,
                    travelMode: 'DRIVING',
                    strokeColor: '#131540',
                    strokeOpacity: 0.6,
                    strokeWeight: 6,
                    callback: function (e) {
                        var ct=0;
                        routeList=[];
                        $.each(e.legs,function(i,leg){
                            var updateAddress=function( address, idx ){
                                var pos = me.map().markers[idx].getPosition();
                                me.map().markers[idx].address=address;
                                routeList.push({
                                    address:address,
                                    lat:pos.lat(),
                                    lng:pos.lng(),
                                    icon:me.icon[idx]
                                });
                            }
                            updateAddress(leg.start_address, ct);
                            ct++;
                            if(i == (e.legs.length-1) && me.map().markers.length>1){
                                updateAddress(leg.end_address, ct);
                                ct++;
                            }
                        });
                        me.updateInfoWindow();
                        if (typeof(callback) == "function")
                            callback(routeList);

                    }
                });

            };



            /**
             *
             * @param marker when is undefined all markers are updated
             */
            this.updateInfoWindow=function(rota){
                openInfoWindow= typeof(openInfoWindow)=="undefined" ? false : openInfoWindow;

                var updateInfo=function(marker,i){
                    var html="";
                    var routeInfo = routeList.length>i ? routeList[i] : "";
                    if(rota==undefined) {
                        html = '<div style="max-width:200px;">' + routeInfo.address + '</div>';
                    }else{
                        html='<ion-slides  options="options" slider="data.slider">'+
                            '<ion-slide-page>'+
                            '<div class="box ">';
                        $.each(rota.Pontos[i].Fotos,function(i,foto){
                            html+='<img href="'+foto.url+'">';
                        });

                        html+='</div>'+
                            '</ion-slide-page>'+
                            '</ion-slides>';
                        html = '<div style="width: 251px;height: 184px;overflow: hidden;position: relative;">'+html+"</div>";
                    }
                    if(marker.infoWindow==undefined) {
                        marker.infoWindow = new google.maps.InfoWindow({ content: html });
                    }else
                        marker.infoWindow.setContent(html);

                }

                $.each(me.map().markers, function (i, marker) {
                    updateInfo(marker,i);
                })

            }

            this.getMyGeoLocation=function(callback) {
                function returnPosition(position) {
                    if(typeof(callback)=="function")
                        callback({lat:position.coords.latitude, lng:position.coords.longitude});
                }
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(returnPosition);
                } else if(typeof(callback)=="function"){
                    return null;
                }
            }


            this.fitRoute = function () {
                if(this.map().markers.length>1)
                    this.map().fitZoom();
                else if(this.map().markers.length>0){
                    var e = this.map().markers[0].position;
                    this.map().setCenter(e.lat(), e.lng());
                }
            }

            this.deleteMarker=function(marker)
            {
                if(!me.isEditable()) return;

                routeService.deleteMarker(me.map().markers.indexOf(marker),function(){
                    delete me.icon[me.map().markers.indexOf(marker)];
                    me.map().removeMarker(marker);
                    me.drawRoute(function(){
                        $rootScope.$emit('mapChanged',routeService.id(),'deleteMarker');
                    });
                })

            }

            this.setMarkerIcon=function(marker,icon,save){
                save= (save==undefined)?true:save;
                var i=this.map().markers.indexOf(marker);
                this.icon[i]=icon;
                //update routeList
                if(typeof(routeList.length)=="number" && routeList.length>i)
                    routeList[i].icon=icon;

                me.updateMarkers();
                if(save==true)
                    $rootScope.$emit('mapChanged',routeService.id(),'setMarkerIcon');
                //me.storeRoutePoints();
            }

            this.addMarker = function(lat,lng, save){
                save= save==undefined?true:save;
                var marker=this.map().addMarker({
                    lat: lat,
                    lng: lng,
                    Title: "Ponto: " + (me.map().markers.length + 1),
                    draggable: me.isEditable(),
                    animation: google.maps.Animation.DROP,
                    dragend: function (e) {
                        //me.geocodeLatLng(e.latLng.lat(), e.latLng.lng(),this);
                        me.drawRoute(function(){
                            $rootScope.$emit('mapChanged',routeService.id(),'markerDraged');
                        });
                    },
                    dblclick: function (e) {
                        me.deleteMarker(this);
                    },
                    click: function (e) {
                        if(!me.isEditable()){


                            /*window.setTimeout(function(){
                             mapObj().find('.flexslider').flexslider({
                             animation: "slide"
                             });
                             },500)*/
                        }
                    }
                });
                this.icon[me.map().markers.indexOf(marker)]='';
                if(save==true) {
                    this.drawRoute(function(){
                        $rootScope.$emit('mapChanged', routeService.id(), 'addMarker');
                    })
                }
                return marker;
            }


            this.zoom = function( zoomLevel ) {
                if (zoomLevel != undefined && parseInt(zoomLevel) >= 6)
                    this.map().map.setZoom(parseInt(zoomLevel));
                return this.map().map.getZoom();
            }

            this.applyServerData=function( data,callback ){

                $timeout(function(){
                    me.clearRoute();
                    $.each(data.Pontos,function(i,ponto){
                        var marker=me.addMarker(ponto['lat'],ponto['lng'] ,false);
                        me.setMarkerIcon(marker,ponto['icon'],false);
                    })
                    me.drawRoute(function(){
                        $timeout(function(){
                            $(window).trigger('resize');
                            me.fitRoute();
                            setupContextMenus();
                            if (typeof(callback) == "function")
                                callback(data);
                        },50);
                    });
                },500);
            }

            //this.marker2SelectIcon =null;


            var selectIconOption={
                title:'Alterar Icon',
                name : 'select_icon',
                action: function(e){
                    //me.marker2SelectIcon = e.marker;
                    $rootScope.$emit('selectIconToMarker',routeService.id(), e.marker);
                    //jQuery("#icon-menu").show();
                    //jQuery("#disableBox").show();
                }
            };


            var mySelectIconWindow=function(message) {
                $ionicPopup.alert({
                    title: 'Message',  // String. The title of the popup.
                    template: message, // String (optional). The html template to place in the popup body.
                    okText: 'Fechar', // String (default: 'OK'). The text of the OK button.

                });
            }

            var zoomOption={
                title:new function(){
                    this.toString=function(){
                        var ret=
                            '<div style="position:relative; margin:auto; width:100%;padding: 3px">'+
                            '<label for="zoomLevel" id="ZoomLabel">Zoom: '+me.zoom()+'</label>'+
                            '<input type="range" value="'+me.zoom()+'" id="zoomLevel" max="19" min="6" step="1" style="width:150px">'+
                            '</div>';
                        return ret;
                    }},
                name : 'zoom_level',
                action: function (e) {
                    me.map().setCenter(e.latLng.lat(), e.latLng.lng());
                    me.zoom(jQuery("#zoomLevel").val());
                }
            };
            jQuery(document).on('input',"input#zoomLevel",function(e){
                jQuery("#ZoomLabel").html('Zoom: '+this.value);
            });

            me.marker2move = null;
            var findPlaceOption={
                title:new function(){
                    this.toString=function()
                    {
                        var ret=
                            '<form id="findPlaceForm" style="position:relative; margin:auto; width:100%;padding: 3px">'+
                            '<input id="findPlace" linked2autocomplete="false" onFocus="geoAutocomplete()" type="text" placeholder="Procura endereço">'+
                            '</form>';
                        return ret;
                    }
                },
                name : 'find_place',
                action: function(e){
                    me.marker2move = e.marker;
                    return false;
                }
            };
            jQuery(document).on('click','input#findPlace',function(ev){
                ev.preventDefault();
            });
            jQuery(document).on('submit','form#findPlaceForm',function(ev){
                ev.preventDefault();
            });
            var autocomplete=null;
            window.geoAutocomplete=function(){
                initAutocomplete();
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }
            var initAutocomplete=function() {
                if( jQuery('input#findPlace').attr('linked2autocomplete')=='false') {
                    autocomplete = new google.maps.places.Autocomplete(
                        jQuery('input#findPlace')[0],
                        {types: ['geocode']})
                    autocomplete.addListener('place_changed', function(){
                        if(autocomplete.getPlace().geometry==undefined)
                            return;
                        var place=autocomplete.getPlace().geometry.location;
                        if(me.isEditable()) {
                            if (me.marker2move != null) {
                                //var latlng = new google.maps.LatLng(place.lat(), place.lng());
                                me.marker2move.setPosition(place);
                                me.marker2move = null;
                                me.drawRoute(function(){
                                    $rootScope.$emit('mapChanged',routeService.id(),'move_marker');
                                })
                            } else {
                                me.addMarker(place.lat(), place.lng(), true);
                            }
                        }
                        me.map().setCenter(place.lat(), place.lng());
                        me.drawRoute();
                    });
                }
            }

            var centerHereOption= {
                title: 'Centrar aqui',
                name: 'center_here',
                action: function (e) {
                    this.setCenter(e.latLng.lat(), e.latLng.lng());
                }
            };

            var centerOnMyLocation={
                title: 'Centrar minha posição',
                name: 'center_here',
                action: function (e) {
                    me.getMyGeoLocation(function(position){
                        me.map().setCenter(position.lat, position.lng);
                    })
                }
            }
            var centerRoute={
                title: 'Centrar rota',
                name: 'center_route',
                action: function (e) {
                    me.fitRoute();
                }
            };

        }
    ])
    .service('routeService',
    ['$http','$ionicPopup','$timeout','loginService','JSONService',
        function( $http,  $ionicPopup,  $timeout,loginService,JSONService){
            var me=this;
            var rota ={Rota:{id:0,descricao: "Nova Rota..."},
                Pontos:[]};

            var setData = function (data) {
                rota = data;
            }

            this.isEditable=function(){
                var ret=false;
                try{
                    // utilizador autenticado
                    ret=loginService.loggedIn() &&
                        (
                            // utilizador authenticado= utilizador dono da rota
                            (loginService.user().id==this.getData().Rota.utilizador_id)
                            ||
                                // rota inda não gravada
                            (this.getData().Rota.id==0)
                        );
                }catch(e){
                    ret=false;
                }
                return ret;
            }
            this.id = function () {
                return this.getData()!=undefined &&
                this.getData().Rota!=undefined
                    ? this.getData().Rota.id
                    : undefined;
            }
            // return route information
            this.getData = function () {
                return rota;
            };

            this.rota = function(){
                return this.getData().Rota;
            }
            this.pontos = function(){
                return this.getData().Pontos;
            }

            this.mergeMapRouteList=function(data,routeList) {
                if(data.Pontos==undefined)
                    data.Pontos=[];
                for(var i=0;i<Math.max(data.Pontos.length,routeList.length);i++){
                    if(i==data.Pontos.length)
                        data.Pontos.push({});
                    if(i<routeList.length) {
                        data.Pontos[i].address = routeList[i].address;
                        data.Pontos[i].lat     = routeList[i].lat;
                        data.Pontos[i].lng     = routeList[i].lng;
                        data.Pontos[i].icon    = routeList[i].icon;
                        data.Pontos[i].order   = i;
                    }else{
                        delete data.Pontos[i];
                    }
                };
                return data;
            }

            var myAlert=function(message) {
                $ionicPopup.alert({
                    title: 'Message',  // String. The title of the popup.
                    template: message, // String (optional). The html template to place in the popup body.
                    okText: 'Fechar', // String (default: 'OK'). The text of the OK button.

                });
            }
            // load route information from server and set data
            this.load = function (id, callback) {

                $http.get(window.getHost()+"/rota2/get?id=" + id)
                    .success(function (data,p2) {
                        if (data['message'] != 'ok') {
                            if (typeof(data['message']) == 'string')
                                data['message'] = [data['message']];
                            var m='';
                            jQuery.each(data['message'], function (i, mess) {
                                m=(m==''?m:'<br>'+m)+mess;
                            });

                            myAlert(m);
                        } else {
                            setData(data.data);
                            if (typeof(callback) == "function")
                                callback(data.data);
                        }
                    })
                    .error(function (data, error) {
                        if(typeof(data.message)!='undefined')
                            myAlert(data.message);
                    });

            }


            /**
             *
             */
            this.putName = function(data,callback) {

                JSONService.post(window.getHost()+"/rota2/addupdate",data,
                    function(data){
                        setData(data.data);
                    });

            }
            /**
             *  async update rota data structure
             * @param data
             * @param callback
             */
            this.save=function(data,callback){
                if(data.Rota.id==0) {
                    if (typeof(callback) == "function")
                        callback(data);
                    return;
                }

                JSONService.post(window.getHost()+"/rota2/update",data,
                    function(data) {
                        setData(data.data);
                    });

            };

            /**
             * async delete foto defined on data param , after delete callback is invoked
             * @param data
             * @param callback
             */
            this.deleteFoto=function(data,callback) {
                if(data.Rota.id==0) {
                    if (typeof(callback) == "function")
                        callback(data);
                    return;
                }

                JSONService.post(window.getHost()+'/rota2/deletefoto', data,
                    function (data) {
                    });
            }

            /**
             * async insert fotos names
             * @param fotos
             * @param callback
             */
            this.insertFotos=function( fotos, callback){
                if(this.rota().id==0) {
                    if (typeof(data.message) != 'undefined')
                        myAlert("Altere o nome da rota para poder adicionar fotos.");
                    return;
                }
                JSONService.post(window.getHost()+'/rota2/insertfotos',fotos,
                    function (data) {

                    });
            }

            /**
             * async update marker [ponto]  history/description
             * @param data
             * @param callback
             */
            this.updateHistory=function( data, callback)
            {
                // rota_id==0 ou ponto_id não definido
                if(this.rota().id==0 || data.id==undefined) {
                    if (typeof(data.message) != 'undefined')
                        myAlert("Altere o nome da rota para poder definir descrição.");
                    return;
                }
                JSONService.post(window.getHost()+'/rota2/updatehistoria',data,
                    function (data) {
                    });
            }

            /**
             * async delete marker
             * @param rota_id
             * @param ponto_order
             */
            this.deleteMarker=function(rota_id, ponto_order)
            {
                if(!me.isEditable()) return;
                if(rota_id==0){
                    try{
                        delete mapService.map().markers[ponto_order-1];
                    }catch(e){}
                    if (typeof(callback) == "function")
                        callback();
                }

                JSONService.get(window.getHost()+"/rota2/deletepoint?id="+rota_id+"&order="+ponto_order,
                    function (data) {
                    });

            }

        }])
    .service('loginService',
    ['$window','$rootScope','$state','JSONService',
        function( $window,  $rootScope, $state, JSONService){
            var user={authenticated: false};

            this.user=function(){
                try {
                    return JSON.parse($window.sessionStorage.user) || user;
                }catch(e){
                    return {authenticated: false};
                }
            }

            this.loggedIn = function() {
                return  typeof($window.sessionStorage)!='undefined' &&
                    typeof($window.sessionStorage.access_token)!='undefined' &&
                    Boolean($window.sessionStorage.access_token);
            };
            this.logout = function () {
                delete $window.sessionStorage.access_token;
                delete $window.sessionStorage.user;
                user={authenticated: false};
                $rootScope.$broadcast('loginChanged','logout');
            };

            this.login=function( username, password, okCallback, errorCallback ) {
                if(typeof(username)=='object'){
                    errorCallback = username.error;
                    okCallback = username.success;
                    password = username.password;
                    username = username.username;
                }

                var data= { username:username,
                    password:password};

                JSONService.post(window.getHost()+'/rota2/login',data,
                    function (data) { /*successCallback*/
                        if (typeof(okCallback) == 'function') {
                            user.id = data.data.id;
                            user.username = data.data.nome;
                            user.email = data.data.email;
                            user.type = data.data.tipoUtilizador;
                            user.authenticated = true;
                            delete user.password;
                            $window.sessionStorage.access_token = data.access_token;
                            $window.sessionStorage.user = JSON.stringify(user);
                            // callback ok
                            okCallback(data, status);
                            $rootScope.$broadcast('loginChanged','login');
                        }
                    }
                    ,
                    function(error) {
                        if(typeof(errorCallback)=='function')
                        // callback error
                            errorCallback(error);
                    });

                //errorCallback("Invalid authentication server response.");

            }
        }]);

