


angular.module('app.controllers', [])
/*******************************************************************
 App - CONTROLLER
 **********************************************************************/
    .controller('appCtrl',
    ['$ionicSideMenuDelegate','$rootScope','$scope','loginService','routeService','$state','$stateParams','$location',
        function( $ionicSideMenuDelegate,  $rootScope,  $scope,  loginService,  routeService,$state,$stateParams,$location) {
            $scope.loggedIn= loginService.loggedIn();
            var listener1 = $rootScope.$on('loginChanged',function(){
                $scope.loggedIn = loginService.loggedIn();
              //  $scope.$broadcast('$destroy');
                $state.go('menu.inicio/:id',{id:getID()},{reload:true});
            });

            function getID(){
                if(typeof($stateParams.id)=="undefined")
                    return 0
                else
                    return $stateParams.id;
            }

            $scope.$on('$destroy', function() {
                listener1();
            });
            $rootScope.logout=function(){
                loginService.logout();
                $ionicSideMenuDelegate.toggleLeft();

                
                //$location.path('/side-menu/inicio/'+getID()).replace();
              //  $scope.loggedIn = loginService.loggedIn();
              //$scope.$broadcast('$destroy');
              //  $state.go('menu.inicio/:id',{id:getID()},{reload:true});
            }
        }])
/*******************************************************************
 Inicio - CONTROLLER
 **********************************************************************/
    .controller('inicioCtrl',[
        '$rootScope','$scope','$state','loginService','routeService','$timeout','mapService','$stateParams',
        function($rootScope,  $scope, $state, loginService,  routeService,  $timeout,  mapService,  $stateParams) {


            function getID(){
                return $stateParams.id;
            }

            var listener1 = $scope.$on('$ionicView.enter', function(ev) {
                if(ev.targetScope !== $scope)
                    return;
                jQuery(window).trigger('resize');
                console.log("App view (menu) entered.");
            });

            var listener2 = $scope.$on('$ionicView.leave', function(ev){ //This just one when leaving, which happens when I logout
                if(ev.targetScope !== $scope)
                    return;
                console.log("App view (menu) leaved.");
            });

            /* rootScope user */
            $scope.loggedIn= loginService.loggedIn();
            var listener3 = $rootScope.$on('loginChanged',function(){
                $scope.loggedIn = loginService.loggedIn();
                if(!$scope.loggedIn) {
                    var resetScope = {
                        data: {
                            Rota: {
                                id: parseInt('x'),
                                descricao: "Nova Rota..."
                            },
                            Pontos: []
                        }
                    };
                    setData(resetScope);
                }
            });
            var listener4=$rootScope.$on('reload',function(e,id){
                id= id==undefined?getID():id;
                load(id,false);
            });
            var listener5=$rootScope.$on('mapChanged',function(e,rota_id,type){
                if(rota_id!=getID()) return;
                if(type!='deleteMarker')
                    save();
            });

            var listener6=function(){};
            $scope.$on('$destroy', function() {
                listener1();
                listener2();
                listener3();
                listener4();
                listener5();
                listener6()
                mapService.releaseMap();
            });

            $scope.showRotaDescricao=function(){
                $rootScope.$emit('novaRotaFormShow');
            }


            $scope.pontoAddress=function(ponto){
                ponto=mapService.updatePonto(ponto);
                return (typeof(ponto.address)=="undefined")
                    ? "Lat: "+ponto.lat+" Lng: "+ponto.lng
                    : ponto.address;
            }

            $scope.editMarkerDetail=function(ponto){
                //open modal
                $scope.ponto=mapService.updatePonto(ponto);
                $state.go('menu.pontos/:id',
                    {order : $scope.ponto.id});
                $timeout(function(){
                    $rootScope.$emit('loadPonto',$scope.ponto);
                },500);
            }

            $scope.inc=function(val){
                return parseInt(val)+1;
            }

            /**
             * async set controller data, after  what callback is invoked
             * @param data
             * @param callback
             */
            var setData=function(data,callback){
                $timeout(function(){
                    $scope.$apply(function(){
                        $scope.rota = data;
                        $scope.pontos = data.Pontos;
                    });
                    $timeout(function(){
                        if (typeof(callback) == "function")
                            callback();
                    },50);

                },150);

            }

            var load=function(id, updateMap,callback) {
                updateMap = typeof(updateMap)=="undefined"?true:updateMap;
                id=id==undefined || isNaN(id)||(id==null) ? getID() : id;

                if(id==0) {
                    mapService.applyServerData($scope.rota,function(){
                        mapService.getMyGeoLocation(function (position) {
                            mapService.map().setCenter(position.lat, position.lng,function(){
                                if (typeof(callback) == "function")
                                    callback(addresses);
                            });
                        });
                    });
                    return;
                }
                routeService.load(id, function (data) {
                    $rootScope.$emit('routeLoaded',data);
                    if(updateMap)
                        mapService.applyServerData(data,function(){
                            if (typeof(callback) == "function")
                                callback(data);
                        });

                    if (mapService.map().markers.length == 0) {
                        setData(data,function(){
                            if (typeof(callback) == "function")
                                callback(data);
                        });
                    } else if (mapService.map().markers.length > 0) {
                        mapService.drawRoute(function (routeList) {
                            data = routeService.mergeMapRouteList(data, routeList);
                            setData(data,function(){
                                if (typeof(callback) == "function")
                                    callback(data);
                            });
                        });
                    } else {
                        mapService.getMyGeoLocation(function (position) {
                            mapService.map().setCenter(position.lat, position.lng);
                            setData(data,function(){
                                if (typeof(callback) == "function")
                                    callback(data);
                            });
                        });
                    }

                });
            }


            var save=function() {
                //merge routeData with mapService data
                //save data
                var data=routeService.mergeMapRouteList($scope.rota, mapService.getRouteList());
                routeService.save(data,function(){
                    $scope.rota=data;
                    $rootScope.$emit('reload');
                })
            };


            // handle save historia
            var myTimeOut=null;
            $scope.previousDescription='';
            $scope.putName=function(event,newName){
                if(typeof(event)=='undefined') return;

                var saveRotaId=$scope.rota.Rota.id;
                var internal_putName=function(){
                    if($scope.previousDescription==newName) return;
                    $scope.previousDescription=newName;
                    var data = {
                        descricao: newName, //$scope.rota.Rota.descricao,
                        id: $scope.rota.Rota.id
                    };
                    routeService.putName(data, function (data) {
                        if(saveRotaId==0){
                            var reloadMap=function(id) {
                                $scope.$broadcast('$destroy');
                                $state.go('menu.inicio/:id', {id: id}, {reload: true});
                            }
                            if(mapService.map().markers.length>0) {
                                var data = routeService.mergeMapRouteList(data, mapService.getRouteList());
                                routeService.save(data, function (data) {
                                    reloadMap(data.Rota.id);
                                });
                            }else {
                                reloadMap(data.Rota.id);
                            }
                        }

                    })
                }

                if (myTimeOut != null)
                    $timeout.cancel(myTimeOut);
                myTimeOut = $timeout(function () {
                    internal_putName();
                    myTimeOut = null;
                }, 500);

            }



            $scope.initMap=function()
            {
                $scope.rota = routeService.getData();
                $scope.pontos = $scope.rota.Pontos;

                $timeout(function () {
                    function lancaFormNovaRota() {
                        load(getID());
                        if (isNaN(getID()) || getID()== null || getID()==0) {
                            $rootScope.$emit('novaRotaFormShow');
                        }
                    }

                    listener6=$rootScope.$on('reload', function (event,id) {
                        id=(id==undefined) ? getID : id;
                        load(id, false);
                    });

                    lancaFormNovaRota();
                }, 500);
            }

        }])

/*******************************************************************
 delete file - CONTROLLER
 **********************************************************************/
    .controller('FileDestroyCtrl', [
        '$scope','routeService',
        function ($scope,routeService) {
            var file = $scope.file,
                state;
            if (file.url) {
                file.$state = function () {
                    return state;
                };
                file.$destroy = function () {
                    state = 'pending';
                    var data = {ficheiro: file.name, ponto_id: file.ponto_id};
                    routeService.deleteFoto( data, function(data){
                        $scope.clear(file);
                        state = 'resolved';
                    })
                }
            } else if (!file.$cancel && !file._index) {
                file.$cancel = function () {
                    $scope.clear(file);
                };
            }
        }
    ])
/*******************************************************************
 pontos - CONTROLLER
 File upload - CONTROLLER
 **********************************************************************/
    .controller('pontosController', [
        '$rootScope','$scope','routeService','mapService','$stateParams','$timeout',
        function ($rootScope,$scope,routeService,mapService,$stateParams,$timeout) {
            $scope.tinymceOptions = {
                menu : {},
                language_url : 'js/tinymce/langs/pt_PT.js',
                resize: true,
                plugins: 'textcolor',
                toolbar: "undo redo styleselect bold italic print forecolor backcolor"

            };

            $scope.$watch('ponto',function(newval,oldval){
                console.log('ponto changed');
            });

            $scope.$watch('ponto.historia',function(newval,oldval){
                console.log('ponto.historia changed');
                if(oldval!=undefined && newval!=oldval)
                    $scope.save(newval);
            })

            var listener2=$rootScope.$on('selectIconToMarker',function(event,rota_id,marker){
                selectedMarker = marker;
            });

            $scope.$on('$destroy', function() {
                listener1();
                listener2();
            });

            // handle save historia
            var myTimeOut=null;
            var lastSave=routeService.pontos();
            $scope.save = function(hist) {
                var send2Server=function(){
                    var sendData = {
                        id:$scope.ponto.id,
                        historia: hist
                    };
                    if(lastSave!=hist)
                        routeService.updateHistory(sendData,function(data){
                            lastSave=sendData.historia;
                        })

                }

                if (myTimeOut != null)
                    $timeout.cancel(myTimeOut);
                myTimeOut = $timeout(function () {
                    send2Server();
                    myTimeOut = null;
                }, 500);
            }

            var selectedMarker=null;

            $scope.inc=function(val){
                return parseInt(val)+1;
            }

            $scope.url=function(name, size, showText){
                var ret = gooIconsService.getUrl( name, size, showText);
                return ret;
            }


            var listener1 = $rootScope.$on('loadPonto',function(ev,data){
                $scope.ponto = data;
                $scope.loadFotos(data.Fotos);
            })
            /********************************************************/
            $scope.options = {
                url: window.getHost()+'/upload/'
            };

            $scope.loadingFiles=true;
            $scope.queue=[];
            $scope.loadFotos=function(fotos)
            {
                $scope.$apply(function(){
                    $scope.queue = fotos;
                    $rootScope.$emit('replaceFileUploadQueue',fotos);
                    $scope.loadingFiles=false;
                })

            }



            var listener2=$scope.$on('fileuploadalways',function(e,data){
                // save file data
                var fotos=[];
                $(data.files).each(function(i,file){
                    fotos.push({
                        ponto_id: e.currentScope.ponto.id,
                        ficheiro: file.name,
                        historia:''
                    });
                });

                routeService.insertFotos( {fotos:fotos},function(data){

                });

            })

            $scope.$on('$destroy', function() {
                listener1();
                listener2();
            });

        }
    ])
/*******************************************************************
 SELECT MARKER ICONS - CONTROLLER
 **********************************************************************/
    .controller('icon_selectCtrl',['$scope','$rootScope','gooIconsService',
        '$ionicModal','mapService',
        function($scope,$rootScope,gooIconsService,$ionicModal,mapService){

            var selectedMarker=null;
            $ionicModal.fromTemplateUrl('icons-window.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
            });
            $scope.icons=gooIconsService.icons;


            $scope.selectIcon=function(icon){
                //set icon 2 marker
                mapService.setMarkerIcon(selectedMarker,icon);
                //close modal
                $scope.closeModal();
            }

            $scope.openModal = function() {
                $scope.modal.show();
            };
            $scope.closeModal = function() {
                $scope.modal.hide();
            };

            // Execute action on hide modal
            var listener1=$scope.$on('modal.hidden', function() {
                // Execute action
            });
            // Execute action on remove modal
            var listener2=$scope.$on('modal.removed', function() {
                // Execute action
            });
            var listener3=$rootScope.$on('selectIconToMarker',function(event,rota_id,marker){
                selectedMarker = marker;
                $scope.openModal();
            });

            // Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function() {
                listener1();
                listener2();
                listener3();
            });
            $scope.url=function(name, size, showText){
                var ret = gooIconsService.getUrl( name, size, showText);
                return ret;
            }
        }])
/******************************************************************
 R O T A S - C O N T R O L L E R
 /******************************************************************/

    .controller('rotasCtrl',
    ['$rootScope','$scope','loginService','routeListService','$state',
        function( $rootScope,  $scope,  loginService,  routeListService,  $state) {
            $scope.openRoute=function( routeId ){
                $state.go('menu.inicio/:id',{id:routeId});
            }

            var load=function(){
                routeListService.load(function(data){
                    $scope.rotas=data;
                });
            }

            var listener1=$rootScope.$on('routeListChanged',function(){
                load();
            });

            $scope.$on('$destroy', function() {
                listener1();
            });

            load();
        }])
/******************************************************************
 sair - C O N T R O L L E R
 /******************************************************************/
    .controller('sairCtrl',
    ['$rootScope','$scope','loginService','routeService',
        function( $rootScope,  $scope,  loginService,  routeService) {
            $rootScope.logout();
            $scope.loggedIn = loginService.loggedIn();
            $rootScope.$broadcast('logout');
        }])
/******************************************************************
 entrar - C O N T R O L L E R
 /******************************************************************/
    .controller('entrarCtrl',
    ['$rootScope','$scope','loginService','$state','routeService','$ionicPopup','$stateParams','$timeout',
        function( $rootScope,  $scope,  loginService,  $state,  routeService,  $ionicPopup,$stateParams,$timeout) {
            $scope.loggedIn= loginService.loggedIn();
            var listener1=$rootScope.$on('loginChanged',function(){
                $scope.loggedIn = loginService.loggedIn();
            });

            $scope.$on('$destroy', function() {
                listener1();
            });

            function getID(){
                if(typeof($stateParams.id)=="undefined")
                    return 0
                else
                    return $stateParams.id;
            }

            $rootScope.user=function(){
                loginService.user();
            }

            $rootScope.$watch('loggedIn',function(newval,oldval){
                console.log('loggerIn state changed to: '+newval);
            });
            $scope.user = loginService.user();

            $scope.login=function(){
                loginService.login(
                    {
                        username: $scope.user.username,
                        password: $scope.user.password,
                        success: function (data, status) {
                            $timeout(function(){
                                $rootScope.$apply(function(){
                                    loggedIn= loginService.loggedIn();
                                });
                            },25);
                            $state.go('menu.inicio/:id',{id:getID()});
                            $scope.user.password="";
                        },
                        error: function (message) {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Login failed!',
                                template: message
                            });
                        }
                    });
            }

            /*if(loginService.loggedIn()) {
             $state.go('menu.inicio/:id',{id:getID()});
             }*/

        }])
/*******************************************************************
 Subcrição - CONTROLLER
 **********************************************************************/
    .controller('subscriOCtrl',
    ['$rootScope','$scope','loginService','routeService',
        function( $rootScope,  $scope,  loginService,  routeService) {
            $scope.loggedIn= loginService.loggedIn();
            var listener1=$rootScope.$on('loginChanged',function(){
                $scope.loggedIn = loginService.loggedIn();
            });

            $scope.$on('$destroy', function() {
                listener1();
            });

        }])
/*******************************************************************
 Opções - CONTROLLER
 **********************************************************************/
    .controller('opEsCtrl',
    ['$rootScope','$scope','loginService','routeService',
        function( $rootScope,  $scope,  loginService,  routeService) {
            $scope.loggedIn= loginService.loggedIn();
            var listener1=$rootScope.$on('loginChanged',function(){
                $scope.loggedIn = loginService.loggedIn();
            });

            $scope.$on('$destroy', function() {
                listener1();
            });
        }])

