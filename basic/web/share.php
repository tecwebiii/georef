<?php
/**
 * Created by PhpStorm.
 * User: vao
 * Date: 20/04/2016
 * Time: 22:17
 */


?>
<script>
(function () {
    tryModule('routeApp').controller('shareController',
        ['$scope','$location','$timeout',
            function($scope,$location,$timeout) {

                $scope.publicUrl=function(){

                    return window.routeMap==undefined
                        ? window.location.href
                        :(window.location.protocol + "//" +
                        window.location.hostname + "/rotas/ver?id=" +
                        window.routeMap.getRotaId());
                }

                $scope.texto="Partilhe tambem as suas viagens ou os seus passeios";
            }]);
}());
</script>
<div id="share-buttons" data-ng-controller="shareController">
    <!-- Facebook -->
    <a ng-href="http://www.facebook.com/sharer.php?u={{ publicUrl() }}" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
    </a>

    <!-- Google+ -->
    <a ng-href="https://plus.google.com/share?url={{ publicUrl() }}" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google" />
    </a>

    <!-- LinkedIn -->
    <a ng-href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ publicUrl() }}" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />
    </a>

    <!-- Tumblr-->
    <a ng-href="http://www.tumblr.com/share/link?url={{ publicUrl() }}&amp;{{ texto }}" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/tumblr.png" alt="Tumblr" />
    </a>

    <!-- Twitter -->
    <a ng-href="https://twitter.com/share?url={{ publicUrl() }}&amp;text={{ texto }}&amp;hashtags="rotas" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
    </a>

    <!-- Yummly -->
    <a ng-href="http://www.yummly.com/urb/verify?url={{ publicUrl() }}&amp;title={{ texto }}" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/yummly.png" alt="Yummly" />
    </a>

</div>
