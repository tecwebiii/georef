/**
 * Created by vao on 22/03/2016.
 */


window.routeMap=null;
initMap=function(){
    var urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    }

    if(window.routeMap==null && typeof(google)=="object") {
        window.routeMap = new RouteMap('#map', '#instructions #steps', '#description');
    }
}


window.gooIcons = new function (){
    this.icons = [
        {icon: 'academy', name: 'Academia'},
        {icon: 'activities', name: 'Actividades'},
        {icon: 'airport', name: 'Aeroporto'},
        {icon: 'amusement', name: 'Divertimento'},
        {icon: 'aquarium', name: 'Aquario'},
        {icon: 'art-gallery', name: 'Galeria de Arte'},
        {icon: 'baby', name: 'Bebe'},
        {icon: 'bank-dollar', name: 'Dollar'},
        {icon: 'bank-euro', name: 'Euro'},
        {icon: 'bank-pound', name: 'Libra'},
        {icon: 'bar', name: 'Bar'},
        {icon: 'barber', name: 'Barbeiro'},
        {icon: 'beach', name: 'Praia'},
        {icon: 'beer', name: 'Cerveja'},
        {icon: 'bicycle', name: 'Bicicleta'},
        {icon: 'books', name: 'Livros'},
        {icon: 'bowling', name: 'Bowling'},
        {icon: 'bus', name: 'Autocarro'},
        {icon: 'cafe', name: 'Café'},
        {icon: 'camping', name: 'Campismo'},
        {icon: 'car-dealer', name: 'Venda de carros'},
        {icon: 'car-rental', name: 'Aluguer de carros'},
        {icon: 'car-repair', name: 'Reparação de carros'},
        {icon: 'casino', name: 'Casino'},
        {icon: 'caution', name: 'Cuidado'},
        {icon: 'cemetery-grave', name: 'Cemiterio'},
        {icon: 'cinema', name: 'Cinema'},
        {icon: 'civic-building', name: 'Construção civil'},
        {icon: 'computer', name: 'Computador'},
        {icon: 'corporate', name: 'Corporativo'},
        {icon: 'courthouse', name: 'Tribunal'},
        {icon: 'fire', name: 'Fogo'},
        {icon: 'flag', name: 'Bandeira'},
        {icon: 'floral', name: 'Florar'},
        {icon: 'helicopter', name: 'Helicoptero'},
        {icon: 'home', name: 'Casa'},
        {icon: 'info', name: 'Informação'},
        {icon: 'landslide', name: 'Desmoronamento'},
        {icon: 'locomotive', name: 'Locomotiva'},
        {icon: 'medical', name: 'Medico'},
        {icon: 'mobile', name: 'Movel'},
        {icon: 'motorcycle', name: 'Motociclo'},
        {icon: 'music', name: 'Musica'},
        {icon: 'parking', name: 'Parque Automovel'},
        {icon: 'pet', name: 'Animal de Estimação'},
        {icon: 'petrol', name: 'Combustivel'},
        {icon: 'phone', name: 'Telefone'},
        {icon: 'picnic', name: 'Picnic'},
        {icon: 'postal', name: 'Correio'},
        {icon: 'repair', name: 'Reparação'},
        {icon: 'restaurant', name: 'Restaurante'},
        {icon: 'sail', name: 'Navegar'},
        {icon: 'school', name: 'Escola'},
        {icon: 'ship', name: 'Navio'},
        {icon: 'shoppingcart', name: 'Compras'},
        {icon: 'ski', name: 'Esqui'},
        {icon: 'snow', name: 'Neve'},
        {icon: 'sport', name: 'Desporto'},
        {icon: 'star', name: 'Estrela'},
        {icon: 'swim', name: 'Nadar'},
        {icon: 'taxi', name: 'Taxi'},
        {icon: 'train', name: 'Comboio'},
        {icon: 'truck', name: 'Camião'},
        {icon: 'wc-female', name: 'WC-Femenino'},
        {icon: 'wc-male', name: 'WC-Masculino'},
        {icon: 'wc', name: 'WC-Casa de banho'},
        {icon: 'wheelchair', name: 'Cadeira de rodas'}
    ];
    this.icons.sort(function(a, b){
        return (a.name > b.name) ? 1 : (a.name< b.name) ? -1 : 0;
    });

    this.getName=function(icon_name) {
        var icon = this.icons.filter(function(i){
            return i.icon==icon_name;
        });
        return (icon.length==0?'':icon[0].name)
    }

    var fontSize=[10,12,16];
    var iconSize=[12,16,24];
    this.getUrl= function( icon_name, size_1_2_3 ){
        //d_simple_text_icon_left
        //chst=<icon_position_string>
        //chld=<text>|<font_size>|<font_fill_color>|<icon_name>|<icon_size>|<icon_fill_color>|<icon_and_text_border_color>

        size_1_2_3=(size_1_2_3!=undefined && size_1_2_3>=1 && size_1_2_3<=3)?size_1_2_3:3;
        var name = this.getName(icon_name);

        return "http://chart.apis.google.com/chart?"+
            "chst="+
            "d_simple_text_icon_left&"+
            "chld=" +
            (name==''?'caution':name) + "|"+  //text
            fontSize[size_1_2_3-1]+"|"+          //font_size
            "000000"+"|"+                       //font_fill_color
            icon_name+"|"+                      //icon_name
            iconSize[size_1_2_3-1]+"|"+         //icon_size [12,16,24]
            "000000"+"|"+                       //icon_fill_color
            "FFFFFF";                           //icon_and_text_border
    }
}


var RouteMap=function( mapObj, instructionObj, descriptionObj)
{
    me=this;
    mapObj=$(mapObj);
    descriptionObj=$(descriptionObj);

    this.icon= [];

    var maxPins = 10;


    this.getRotaId = function(){
        return me.getStoredData().Rota.id;
    }

    this.updateMarkers = function () {
        var backColor = new Rainbow();
        backColor.setNumberRange(1, Math.max(2, this.map.markers.length));
        backColor.setSpectrum('#006600', '#cccc00');
        var fontColor = new Rainbow();
        fontColor.setNumberRange(1, Math.max(2, this.map.markers.length));
        fontColor.setSpectrum('white', 'white');

        var i = 0;
        for (i = 0; i < this.map.markers.length; i++) {
            texto = (i+1)+"";
            icon = this.icon[i];
            var url = (icon=='' || icon==null)
                ? "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + texto + "|" + backColor.colourAt(i) + "|" + fontColor.colourAt(i)
                : "http://chart.apis.google.com/chart?chst=d_bubble_icon_text_small&chld="+icon+"|edge_bc|"+texto+ " |" + backColor.colourAt(i) + "|" + fontColor.colourAt(i);
            this.map.markers[i].setIcon(url);
            var title = "Ponto: " + (i + 1);
            title+=((icon=='' || icon==null)?''
                :', '+gooIcons.getName(icon));
            this.map.markers[i].setTitle(title);
        }
    }



    this.clearRoute = function () {
        this.map.cleanRoute();
        this.map.removeMarkers();
        this.icon= [];
    }

    this.isEditable=function(){
        return window.location.pathname=='/rotas/nova' ||
            window.location.pathname=='/rotas/altera';
    }

    var draw_point_element=function( order , address ){
        var ret='<div class="row ponto_historia" data-order="'+order+'">';
        ret += '  <div class="col-xs-1">'+(order+1)+'</div>';
        ret += '  <div class="col-xs-9">'+address+'</div>';
        ret += '  <div class="col-xs-2"><button class="add_foto_button btn-primary">foto</button></div>';

        ret+="</div>";
        return ret;
    }
    //$(document).on('click','button.add_foto_button',event_handle_add_foto_button);

    var addresses=[];
    this.getAddressList=function(){
        return addresses;
    }

    this.drawRoute = function (callback) {

        if(me.map.markers.length==0)
            return;

        me.updateMarkers();

        var origin = me.map.markers[0].getPosition();
        var destination = me.map.markers[me.map.markers.length - 1].getPosition();
        var i = 0, waypoints = [];
        for (i = 1; i < me.map.markers.length - 1; i++) {
            waypoints.push({
                location: me.map.markers[i].getPosition().lat() + ", " + me.map.markers[i].getPosition().lng(),
                stopover: true
            });
        }

        me.map.cleanRoute();
        me.map.drawRoute({
            origin: [origin.lat(), origin.lng()],
            destination: [destination.lat(), destination.lng()],
            waypoints: waypoints,
            optimizeWaypoints: false,//waypoints.length != 0,
            travelMode: 'DRIVING',
            strokeColor: '#131540',
            strokeOpacity: 0.6,
            strokeWeight: 6,
            callback: function (e) {
                var ct=0;
                addresses=[];
                $.each(e.legs,function(i,leg){
                    var updateAddress=function( address ){
                        addresses.push(address);
                    }
                    updateAddress(leg.start_address);
                    ct++;
                    if(i == (e.legs.length-1) && me.map.markers.length>1){
                        updateAddress(leg.end_address);
                        ct++;
                    }
                });
                me.updateInfoWindow();
                if (typeof(callback) == "function")
                    callback(addresses);

            }
        });

    };



    /**
     *
     * @param marker when is undefined all markers are updated
     */
    this.updateInfoWindow=function(){
        openInfoWindow= typeof(openInfoWindow)=="undefined" ? false : openInfoWindow;

        var updateInfo=function(marker,i){
            var html="";
            var address = addresses.length>i ? addresses[i] : "";
            if(me.isEditable()) {
                html = '<div style="max-width:200px;">' + address + '</div>';
                marker.infoWindow = new google.maps.InfoWindow({ content: html });
            }else{
                html=$(".ponto_historia[data-order="+i+"]");
                if(html.length>0 && marker.infoWindow==undefined) {
                    html=html.clone();
                    html = $(html).css('display', 'block').clone();
                    html = $('<div></div>').append(html).html();
                    html = '<div style="width: 251px;height: 184px;overflow: hidden;position: relative;">'+html+"</div>";
                    marker.infoWindow = new google.maps.InfoWindow({ content: html });
                }
            }

        }

        $.each(me.map.markers, function (i, marker) {
            updateInfo(marker,i);
        })

    }

    /*mapObj.on("click",".slides",function(e){
        $("#allFotosWindow").modal();
    })*/

    this.getMyGeoLocation=function(callback) {
        function returnPosition(position) {
            if(typeof(callback)=="function")
                callback({lat:position.coords.latitude, lng:position.coords.longitude});
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(returnPosition);
        } else if(typeof(callback)=="function"){
            return null;
        }
    }


    this.fitRoute = function () {
        if(this.map.markers.length>1)
            this.map.fitZoom();
        else if(this.map.markers.length>0){
            var e = this.map.markers[0].position;
            this.map.setCenter(e.lat(), e.lng());
        }
    }

    this.deleteMarker=function(marker)
    {
        if(!me.isEditable()) return;
        var data={id:me.getRotaId(),
            order:me.map.markers.indexOf(marker)
        };
        if(isNaN(data.id)){
            return;
        }
        $.ajax({
            type: "GET",
            url: "/rota/deletepoint",
            data: data ,
            dataType: "json",
            success:function(data,textStatus,jqXHR){
                if(data['message']!='ok'){
                    if(typeof(data['message']) == 'string')
                        data['message']=[data['message']];
                    $.each(data['message'],function(i,mess){
                        Lobibox.notify('error', {
                            msg: mess,
                            sound:false,
                            delay:2000,
                            size:'mini'
                        });
                    });
                }else {
                    delete me.icon[me.map.markers.indexOf(marker)];
                    me.map.removeMarker(marker);
                    me.drawRoute();
                }
            },
            error:function(jqXHR,textStatus,error){
                Lobibox.notify('error', {
                    msg: error,
                    sound:false,
                    delay:2000,
                    size:'mini'
                });
            }

        });

    }

    this.setMarkerIcon=function(marker,icon,save){
        save= (save==undefined)?true:save;
        var i=this.map.markers.indexOf(marker);
        this.icon[i]=icon;
        me.updateMarkers();
        if(save==true)
            me.storeRoutePoints();
    }

    this.addMarker = function(lat,lng, save){
        save= save==undefined?true:save;
        var marker=this.map.addMarker({
            lat: lat,
            lng: lng,
            Title: "Ponto: " + (me.map.markers.length + 1),
            draggable: me.isEditable(),
            animation: google.maps.Animation.DROP,
            dragend: function (e) {
                //me.geocodeLatLng(e.latLng.lat(), e.latLng.lng(),this);
                me.drawRoute();
                me.storeRoutePoints();
            },
            dblclick: function (e) {
                me.deleteMarker(this);
            },
            click: function (e) {
                if(!me.isEditable())
                    window.setTimeout(function(){
                        mapObj.find('.flexslider').flexslider({
                            animation: "slide"
                        });
                    },500)
            }
        });
        this.icon[me.map.markers.indexOf(marker)]='';
        //me.geocodeLatLng(lat,lng,marker,save==true?true:false);
        if(save==true)
            me.storeRoutePoints();
        return marker;
    }

    this.map = new GMaps({
        div: mapObj[0],
        lat: 39.391714,
        lng: -9.130700,
        zoom: 16,
        styles: [{
            "elementType": "geometry",
            "stylers": [{"hue": "#ff4400"}, {"saturation": -68}, {"lightness": -4}, {"gamma": 0.72}]
        }, {"featureType": "road", "elementType": "labels.icon"}, {
            "featureType": "landscape.man_made",
            "elementType": "geometry",
            "stylers": [{"hue": "#0077ff"}, {"gamma": 3.1}]
        }, {
            "featureType": "water",
            "stylers": [{"hue": "#00ccff"}, {"gamma": 0.44}, {"saturation": -33}]
        }, {
            "featureType": "poi.park",
            "stylers": [{"hue": "#44ff00"}, {"saturation": -23}]
        }, {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [{"hue": "#007fff"}, {"gamma": 0.77}, {"saturation": 65}, {"lightness": 99}]
        }, {
            "featureType": "water",
            "elementType": "labels.text.stroke",
            "stylers": [{"gamma": 0.11}, {"weight": 5.6}, {"saturation": 99}, {"hue": "#0091ff"}, {"lightness": -86}]
        }, {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [{"lightness": -48}, {"hue": "#ff5e00"}, {"gamma": 1.2}, {"saturation": -23}]
        }, {
            "featureType": "transit",
            "elementType": "labels.text.stroke",
            "stylers": [{"saturation": -64}, {"hue": "#ff9100"}, {"lightness": 16}, {"gamma": 0.47}, {"weight": 2.7}]
        }],
        click: function (e) {
            google.maps.event.trigger(window.routeMap.map.map, 'rightclick',e);
        },
        zoom_changed: function (e) {
            $("#zoomLevel").val( me.zoom() );
            $("#ZoomLabel").html('Zoom: '+me.zoom() );
        },
        idle:function(e){
            $("#zoomLevel").val( me.zoom() );
            $("#ZoomLabel").html('Zoom: '+me.zoom() );
        }

    });

    this.zoom = function( zoomLevel ){
        if(zoomLevel!=undefined &&  parseInt(zoomLevel)>=6  )
            this.map.map.setZoom( parseInt(zoomLevel) );
        return this.map.map.getZoom();
    }

    this.getStoredData=function(){
        return rotaScope().rota.data;
    }

    this.applyServerData=function( data ){
        this.clearRoute();
        $.each(data['data']['Pontos'],function(i,ponto){
            var marker=me.addMarker(ponto['lat'],ponto['lng'] ,false);
            me.setMarkerIcon(marker,ponto['icon'],false);
        })
        me.drawRoute();
        me.fitRoute();
    }

    this.marker2SelectIcon =null;
    this.storeRoutePoints=function() {
        if(!me.isEditable()) return;
        var pontos = [],ponto;
        for (i = 0; i < this.map.markers.length ; i++) {
            ponto={lat: this.map.markers[i].getPosition().lat(),
                lng: this.map.markers[i].getPosition().lng(),
                icon: me.icon[i]};
            pontos.push(ponto);
        }
        var nomeRota=rotaScope().rota.data.Rota.descricao;
        data={Rota:{id:me.getRotaId(),descricao:nomeRota},
            Pontos:pontos};


        $.ajax({
            type: "POST",
            url: "/rota/update",
            data: data ,
            dataType: "json",
            success:function(data,textStatus,jqXHR){
                if(data['message']!='ok'){
                    if(typeof(data['message']) == 'string')
                        data['message']=[data['message']];
                    $.each(data['message'],function(i,mess){
                        Lobibox.notify('error', {
                            msg: mess,
                            sound:false,
                            delay:2000,
                            size:'mini'
                        });
                    });
                }else {
                    rotaScope().$emit('reload');
                }
            },
            error:function(jqXHR,textStatus,error){
                Lobibox.notify('error', {
                    msg: error,
                    sound:false,
                    delay:2000,
                    size:'mini'
                });
            }

        });
    };

    var selectIconOption={
        title:'Alterar Icon',
        name : 'select_icon',
        action: function(e){
            me.marker2SelectIcon = e.marker;
            $("#icon-menu").show();
            $("#disableBox").show();
        }
    };


    if($('body').find('#icon-menu').length==0)
    {
        var html=
            '<div id="disableBox" style="display: none"></div>'+
            '<ul id="icon-menu"  style="display: none">';
        $.each(gooIcons.icons,function(i,icon){
            html += '<li class="icon-option" data-icon_name="'+icon.icon+'">' +
                '<img src="'+gooIcons.getUrl(icon.icon,2)+'"></li>' ;
        });
        html +='</ul>';
        $('body').append(html);
        $('body').on('click','.icon-option',function(e){
            me.setMarkerIcon(me.marker2SelectIcon,$(this).data('icon_name'));
            $("#disableBox").hide();
            $("#icon-menu").hide();
        });
        $('body').on('click','#disableBox',function(e){
            me.setMarkerIcon(me.marker2SelectIcon,$(this).data('icon_name'));
            $("#disableBox").hide();
            $("#icon-menu").hide();
        });
    }


    var zoomOption={
        title:new function(){
            this.toString=function(){
                var ret=
                    '<div style="position:relative; margin:auto; width:100%;padding: 3px">'+
                        /*'<span style="position:absolute; color:red; top:2px;font-size:smaller;min-width:100px;">'+
                         '<span id="zoomLevelOutput"></span>'+
                         '</span>'+*/
                    '<label for="zoomLevel" id="ZoomLabel">Zoom: '+me.zoom()+'</label>'+
                    '<input type="range" value="'+me.zoom()+'" id="zoomLevel" max="19" min="6" step="1" style="width:150px">'+
                    '</div>';
                return ret;
            }},
        name : 'zoom_level',
        action: function (e) {
            me.map.setCenter(e.latLng.lat(), e.latLng.lng());
            window.routeMap.zoom($("#zoomLevel").val());
        }
    };
    $(document).on('input',"input#zoomLevel",function(e){
        $("#ZoomLabel").html('Zoom: '+this.value);
    });

    me.marker2move = null;
    var findPlaceOption={
        title:new function(){
            this.toString=function()
            {
                var ret=
                    '<form id="findPlaceForm" style="position:relative; margin:auto; width:100%;padding: 3px">'+
                    '<input id="findPlace" linked2autocomplete="false" onFocus="geoAutocomplete()" type="text" placeholder="Procura endereço">'+
                    '</form>';
                return ret;
            }
        },
        name : 'find_place',
        action: function(e){
            me.marker2move = e.marker;
            return false;
        }
    };
    $(document).on('click','input#findPlace',function(ev){
        ev.preventDefault();
    });
    $(document).on('submit','form#findPlaceForm',function(ev){
        ev.preventDefault();
    });
    var autocomplete=null;
    window.geoAutocomplete=function(){
        initAutocomplete();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
    var initAutocomplete=function() {
        if( $('input#findPlace').attr('linked2autocomplete')=='false') {
            autocomplete = new google.maps.places.Autocomplete(
                $('input#findPlace')[0],
                {types: ['geocode']})
            autocomplete.addListener('place_changed', function(){
                if(autocomplete.getPlace().geometry==undefined)
                    return;
                var place=autocomplete.getPlace().geometry.location;
                if(me.isEditable()) {
                    if (me.marker2move != null) {
                        //var latlng = new google.maps.LatLng(place.lat(), place.lng());
                        me.marker2move.setPosition(place);
                        me.marker2move = null;
                    } else {
                        me.addMarker(place.lat(), place.lng(), true);
                    }
                }
                me.map.setCenter(place.lat(), place.lng());
                me.drawRoute();
                me.storeRoutePoints();
            });
        }
    }

    var centerHereOption= {
        title: 'Centrar aqui',
        name: 'center_here',
        action: function (e) {
            this.setCenter(e.latLng.lat(), e.latLng.lng());
        }
    };

    var centerOnMyLocation={
        title: 'Centrar minha posição',
        name: 'center_here',
        action: function (e) {
            me.getMyGeoLocation(function(position){
                me.map.setCenter(position.lat, position.lng);
            })
        }
    }
    var centerRoute={
        title: 'Centrar rota',
        name: 'center_route',
        action: function (e) {
            me.fitRoute();
        }
    };

    this.map.setContextMenu({
        control: 'marker',
        options: $.merge(
            (me.isEditable()?[{
                title: 'Apagar Ponto',
                name: 'apagar_ponto',
                action: function (e) {
                    me.deleteMarker(e.marker);
                }}]:[]),
            [centerHereOption,
                centerRoute,
                zoomOption,
                findPlaceOption,
                selectIconOption])
    });
    this.map.setContextMenu({
        control: 'map',
        options: $.merge(
            (me.isEditable()?[{
                title: 'Adicionar ponto',
                name: 'add_marker',
                action: function(e) {
                    me.addMarker( e.latLng.lat(),e.latLng.lng());
                    me.drawRoute();
                    me.storeRoutePoints();
                }}]:[]),
            [centerHereOption,
                centerOnMyLocation,
                centerRoute,
                zoomOption,
                findPlaceOption])
    });

    this.uploadJpg=function() {
        html2canvas($("#map")[0], {
            onrendered: function(canvas) {
                var dataURL = canvas.toDataURL();
                $.ajax({
                    type: "POST",
                    url: "/rota/uploadmapfoto",
                    data: {
                        imgBase64: dataURL,
                        id:me.getRotaId(),
                    }
                }).done(function(o) {
                    console.log('saved');
                });

            },
        });

    }

}
