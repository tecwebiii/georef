/**
 * Created by vao on 23/03/2016.
 */

$(document).ready(function(){
    var ResizeMap= function( mapObj ){
        var me=this;
        var navHeight = $('nav:first').length>0
                ? $('nav:first').height()
                  + parseInt($('nav:first').css('padding-top'))
                  + parseInt($('nav:first').css('margin-top'))
                  + 3
                :0;
        var footerHeight = $('footer:first').length>0
                ? $('footer:first').height()
                + parseInt($('footer:first').css('padding-top'))
                + parseInt($('footer:first').css('margin-top'))
                + 3
                :0;
        this.mapObj = $(mapObj);




        this.calcWidth=function(){
            var availableWidth = $(window).width();
            return availableWidth;
        }

        this.calcHeight=function(){
            var availableHeight = $(window).height()-navHeight-footerHeight;
            return availableHeight;
        }

        me.mapObj.css({
            position:'absolute',
            width:me.calcWidth(),
            left:0,
            top:navHeight,
            height:me.calcHeight(),
            display:'',
        });

        $(window).resize(function() {
            me.mapObj.css({
                width:me.calcWidth(),
                left:0,
                top:navHeight,
                minHeight:me.calcHeight(),
            });
        });
    }
    window.resizeMap= new ResizeMap('#map');

});
