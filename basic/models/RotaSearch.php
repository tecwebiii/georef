<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rota;

/**
 * RotaSearch represents the model behind the search form about `app\models\Rota`.
 */
class RotaSearch extends Rota
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descricao'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'descricao' => Yii::t('app', 'Procura'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rota::find()->joinWith(['pontos','pontos.fotos']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 80,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if(empty($this->descricao))
            $query->where(['rota.id'=>'0']);
        else{
            $query->orFilterWhere(['like', 'rota.descricao', $this->descricao]);
            $query->orFilterWhere(['like', 'fotos.historia', $this->descricao]);
        }
        return $dataProvider;
    }
}
