<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ponto".
 *
 * @property string $id
 * @property double $lat
 * @property double $lng
 * @property string $icon
 * @property string $rota_id
 * @property string $order
 * @property string $historia
 *
 * @property Fotos[] $fotos
 * @property Rota $rota
 */
class Ponto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ponto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lat', 'lng', 'rota_id', 'order'], 'required'],
            [['lat', 'lng'], 'number'],
            [['rota_id', 'order'], 'integer'],
            [['historia'], 'string'],
            [['icon'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'icon' => Yii::t('app', 'Icon'),
            'rota_id' => Yii::t('app', 'Rota ID'),
            'order' => Yii::t('app', 'Order'),
            'historia' => Yii::t('app', 'Historia'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFotos()
    {
        return $this->hasMany(Fotos::className(), ['ponto_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRota()
    {
        return $this->hasOne(Rota::className(), ['id' => 'rota_id']);
    }
}
