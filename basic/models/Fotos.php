<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotos".
 *
 * @property string $id
 * @property string $ponto_id
 * @property string $ficheiro
 * @property string $historia
 * //calculated
 * @property string $name;
 * @property integer $size;
 * @property string $url;
 * @property string $deleteUrl;
 * @property string $deleteType;
 * @property string $thumbnailUrl;
 *
 * @property Ponto $ponto
 */

use yii\web\UploadedFile;
use app\models\Ponto;
use yii\helpers\Url;

class Fotos extends \yii\db\ActiveRecord
{
    var $name='';
    var $size=0;
    var $url='';
    var $deleteUrl='';
    var $deleteType='';
    var $thumbnailUrl='';
    var $calculatedFields=[
            'name',
            'size',
            'url',
            'deleteUrl',
            'deleteType',
            'thumbnailUrl'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ficheiro', 'utilizador_id'], 'required'],
            [['ponto_id', 'utilizador_id'], 'integer'],
            [['historia'], 'string'],
            [['ficheiro'], 'string', 'max' => 150],

            [['size'],  'integer'],
            [['name','url','deleteUrl','deleteType','thumbnailUrl'],'string'],
        ];
    }

    public function getPontoByRotaIdAndOrder(){
        return Ponto::find()->where(['rota_id'=>$this->rota_id,'order'=>$this->order]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ponto_id' => Yii::t('app', 'Ponto ID'),
            'ficheiro' => Yii::t('app', 'Ficheiro'),
            'historia' => Yii::t('app', 'Historia'),
            'utilizador_id' => Yii::t('app', 'Utilizador ID'),
            'name'          => Yii::t('app', 'Nome'),
            'size'          => Yii::t('app', 'Tamanho'),
            'url'           => Yii::t('app', 'Link Imagem'),
            'deleteUrl'     => Yii::t('app', 'Link Remover'),
            'deleteType'    =>Yii::t('app', 'Tipo Remo��o'),
            'thumbnailUrl'  =>Yii::t('app', 'Link Miniatura'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPonto()
    {
        return $this->hasOne(Ponto::className(), ['id' => 'ponto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizador()
    {
        return $this->hasOne(Utilizador::className(), ['id' => 'utilizador_id']);
    }


    /**
     * @return mixed
     */
    public function getUploadFolder(){
        $ret=empty(\Yii::$app->params['uploadFolder'])
            ?  \Yii::getAlias('@webroot').DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.'files'
            : \Yii::$app->params['uploadFolder'];
        return $ret;
    }

    public function getFullPath($fich){
        return $this->getUploadFolder().DIRECTORY_SEPARATOR.
                (!empty($fich)?$fich:$this->ficheiro);
    }

    public function getThumbnailFullPath($fich){
        return $this->getUploadFolder().DIRECTORY_SEPARATOR.'thumbnail'.DIRECTORY_SEPARATOR.
                (!empty($fich)?$fich:$this->ficheiro);
    }

    public function fotoExist($fich){
        return file_exists($this->getFullPath($fich));
    }
    public function getFotoUrl($fich){
        $ret=empty(\Yii::$app->params['uploadFolder'])
            ?  '/upload/files'
            : \Yii::$app->params['uploadFolder'];
        return $ret.'/'.(!empty($fich)?$fich:$this->ficheiro);;
    }

    public function getThumbnailUrl($fich){
        $ret=empty(\Yii::$app->params['uploadFolder'])
            ?  '/upload/files/thumbnail'
            : \Yii::$app->params['uploadFolder'].'/thumbnail';
        return $ret.'/'.(!empty($fich)?$fich:$this->ficheiro);;
    }



    /**
     * Populates an active record object using a row of data from the database/storage.
     *
     * This is an internal method meant to be called to create active record objects after
     * fetching data from the database. It is mainly used by [[ActiveQuery]] to populate
     * the query results into active records.
     *
     * When calling this method manually you should call [[afterFind()]] on the created
     * record to trigger the [[EVENT_AFTER_FIND|afterFind Event]].
     *
     * @param BaseActiveRecord $record the record to be populated. In most cases this will be an instance
     * created by [[instantiate()]] beforehand.
     * @param array $row attribute values (name => value)
     */

    private static function setF( $record,$name, $value )
    {
        if ($record->hasAttribute($name)){
            $record->setAttribute($name, $value);
        } elseif ($record->canSetProperty($name)) {
            $record->$name = $value;
        }
    }

    public function extraFields(){
        $row = $this->toArray();
        $record = $this;
        $fich = $row['ficheiro'];
        return [
            'name'      => $row['ficheiro'],
            "size"      =>file_exists($record->getFullPath($fich))
                                ? filesize($record->getFullPath($fich))
                                : '...',
            "url"       =>$record->getFotoUrl($fich),
            "deleteUrl" =>Url::to('/upload/index.php').'?file='.$row['ficheiro'],
            "deleteType"=>"DELETE",
            "thumbnailUrl"=>(file_exists($record->getThumbnailFullPath($fich))?$record->getThumbnailUrl($fich) : '')
        ];
    }


    public function allFields(){
        $extra= $this->extraFields();
        return array_merge($this->toArray(),$extra);
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fotos';
    }

    public function gps_location(){
        return $this->read_gps_location( $this->getFullPath() );
    }

    /**
     * Returns an array of latitude and longitude from the Image file
     * @param image $file
     * @return multitype:number |boolean
     */
    private function read_gps_location($file){
        if (is_file($file)) {
            $info = exif_read_data($file);
            if (isset($info['GPSLatitude']) && isset($info['GPSLongitude']) &&
                isset($info['GPSLatitudeRef']) && isset($info['GPSLongitudeRef']) &&
                in_array($info['GPSLatitudeRef'], array('E','W','N','S')) && in_array($info['GPSLongitudeRef'], array('E','W','N','S'))) {

                $GPSLatitudeRef  = strtolower(trim($info['GPSLatitudeRef']));
                $GPSLongitudeRef = strtolower(trim($info['GPSLongitudeRef']));

                $lat_degrees_a = explode('/',$info['GPSLatitude'][0]);
                $lat_minutes_a = explode('/',$info['GPSLatitude'][1]);
                $lat_seconds_a = explode('/',$info['GPSLatitude'][2]);
                $lng_degrees_a = explode('/',$info['GPSLongitude'][0]);
                $lng_minutes_a = explode('/',$info['GPSLongitude'][1]);
                $lng_seconds_a = explode('/',$info['GPSLongitude'][2]);

                $lat_degrees = $lat_degrees_a[0] / $lat_degrees_a[1];
                $lat_minutes = $lat_minutes_a[0] / $lat_minutes_a[1];
                $lat_seconds = $lat_seconds_a[0] / $lat_seconds_a[1];
                $lng_degrees = $lng_degrees_a[0] / $lng_degrees_a[1];
                $lng_minutes = $lng_minutes_a[0] / $lng_minutes_a[1];
                $lng_seconds = $lng_seconds_a[0] / $lng_seconds_a[1];

                $lat = (float) $lat_degrees+((($lat_minutes*60)+($lat_seconds))/3600);
                $lng = (float) $lng_degrees+((($lng_minutes*60)+($lng_seconds))/3600);

                //If the latitude is South, make it negative.
                //If the longitude is west, make it negative
                $GPSLatitudeRef  == 's' ? $lat *= -1 : '';
                $GPSLongitudeRef == 'w' ? $lng *= -1 : '';

                return array(
                    'lat' => $lat,
                    'lng' => $lng
                );
            }
        }
        return false;
    }
}
