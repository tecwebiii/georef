<?php

namespace app\models;

use Yii;



/**
 * This is the model class for table "utilizador".
 *
 * @property string $id
 * @property string $nome
 * @property string $senha
 * @property string $authenticationKey
 * @property string $accessToken
 * @property string $email
 * @property string $telefone
 * @property string $tipoUtilizador
 *
 * @property Rota[] $rotas
 */
class Utilizador extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'utilizador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'senha', 'email'], 'required'],
            [['tipoUtilizador'], 'string'],
            [['nome', 'senha', 'authenticationKey', 'accessToken', 'email'], 'string', 'max' => 50],
            [['telefone'], 'string', 'max' => 20],
            [['email'], 'unique'],
            [['nome'], 'unique'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
            [['senha','authenticationKey','accessToken'], 'secret']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nome' => Yii::t('app', 'Nome'),
            'senha' => Yii::t('app', 'Senha'),
            'authenticationKey' => Yii::t('app', 'Authentication Key'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'email' => Yii::t('app', 'Email'),
            'telefone' => Yii::t('app', 'Telefone'),
            'tipoUtilizador' => Yii::t('app', 'Tipo Utilizador'),
            'verifyCode' => Yii::t('app', 'C�digo de Verifica��o'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFotos()
    {
        return $this->hasMany(Fotos::className(), ['utilizador_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRotas()
    {
        return $this->hasMany(Rota::className(), ['utilizador_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    public function save($runValidation = true, $attributeNames = null){
        if(empty($this->authenticationKey)) {
            $this->authenticationKey= Yii::$app->security->generateRandomString();
        }
        return parent::save($runValidation,$attributeNames);
    }
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        if(empty($this->authenticationKey)){
            $this->save(false,['authenticationKey']);
        }
        return $this->authenticationKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authenticationKey === $authKey;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
        //return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne(['authenticationKey' => $token]);
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->senha === $password;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['nome'=>$username])
            ->orWhere(['email'=>$username])->one();
    }
}