<?php

namespace app\models;

use Yii;
use app\models\Ponto;

/**
 * This is the model class for table "rota".
 *
 * @property string $id
 * @property string $descricao
 * @property string $utilizador_id
 *
 * @property Ponto[] $pontos
 * @property Utilizador $utilizador
 * @property Fotos[] $fotos
 */
class Rota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['utilizador_id'], 'integer'],
            [['descricao'], 'string', 'max' => 250],
            [['descricao','utilizador_id'], 'required'],
            [['descricao'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'descricao' => Yii::t('app', 'Descricao'),
            'utilizador_id' => Yii::t('app', 'Utilizador ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPontos()
    {
        return $this->hasMany(Ponto::className(), ['rota_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizador()
    {
        return $this->hasOne(Utilizador::className(), ['id' => 'utilizador_id'])
            ->select(['id','nome','email','telefone','tipoUtilizador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFotos()
    {
        return $this->hasMany(Fotos::className(), ['ponto_id' => 'id'])
            ->viaTable('ponto', ['rota_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSelectedFoto()
    {
        return $this->hasMany(Fotos::className(), ['ponto_id' => 'id'])
            ->viaTable('ponto', ['rota_id' => 'id'])
            ->limit(1);
    }


}
