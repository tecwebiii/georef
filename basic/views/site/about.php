<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use Yii;

$this->title = Yii::t('app','About');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Trabalho de Grupo unidade curricular Tecnologia Web III<br>
        Turma SIWM 2015-2016
    </p>

</div>
