<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $foto app\models\Fotos */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="map"></div>

<div id="left-autohide" ng-controller="routeController" style="display: none">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="description" ng-click="showRotaDescricao()">{{ rota.data.Rota.descricao }}</h3>
    <div class="row">
        <div class="jump2Point col-xs-1"
             data-ng-controller="jumperController"
             ng-repeat="ponto in pontos"
             ng-click="jumpTo(ponto.order)">{{ inc(ponto.order) }}</div>
    </div>
    <div class="container-fluid" style="padding:0px;width:100%;max-height:calc(100% - 130px);overflow-y: scroll">
        <div class="row" id="instructions" data-ng-include="'/listapontos.html'">
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="novaRotaForm" role="dialog" data-ng-controller="novaRotaController">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Nome / Descrição de Rota</h4>
                </div>
                <div class="modal-body">
                    <p>Introduza um nome ou uma breve descrição da rota</p>
                    <input class="myInput" type="text" data-ng-model="rota.data.Rota.descricao" name="nomeRota" id="nomeRota"/>
                    <div ng-if="rotaDescricaoError!=''" style="color:red">{{rotaDescricaoError}}</div>
                </div>
                <div class="modal-footer">
                    <button ng-click="submitForm()" type="button" id="submitNovaRota" class="btn btn-default" data-xdismiss="modal">Enviar</button>
                    <button ng-if="!rota.data.Rota.id" ng-click="cancelForm()" type="button" class="btn btn-default" data-xdismiss="modal">Cancelar</button>
                </div>
            </div>

        </div>
    </div>
</div>

