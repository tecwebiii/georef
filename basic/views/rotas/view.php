<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $foto app\models\Fotos */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="map"></div>

<div id="" ng-controller="routeController">
    <h3 id="description">{{ rota.data.Rota.descricao }}</h3>
    <div class="container-fluid" style="padding:0px;width:100%;max-height:calc(100% - 130px);overflow-y: scroll">
        <div class="row" id="instructions" data-ng-include="'/viewpontos.html'">
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-transparent fade" id="allFotosWindow" role="dialog" style="display:none">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content container-fluid">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Fotos</h4>
                    <h3 class="modal-title">{{ rota.data.Rota.descricao }}</h3>
                </div>
                <div class="row" ng-controller="markController" data-ng-include="'/slider3.html'"></div>
            </div>
        </div>
    </div>
</div>









