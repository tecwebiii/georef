<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RotaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rotas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rota-index">
    <div>
        <h1><?= Html::encode($this->title) ?></h1>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
</div>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'id' => 'rotasLista',
    'itemOptions' => [ 'class' => 'item'],
    'itemView' => function (app\models\Rota $model, $key, $index, $widget) {
        return Html::a(Html::encode($model->descricao), $model->utilizador_id==(Yii::$app->user->isGuest?-1:Yii::$app->user->getId())
                                ?['altera', 'id' => $model->id]
                                :['ver', 'id' => $model->id]);
    },
]) ?>

