<style>
    .modal-list > li > a {
        color:#333;
        text-decoration: none;
        list-style: none;
        display: block;
        text-align: left;
    }
    .modal-content{
        position: relative;
        left:0px;
    }
</style>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Sign up or log in using OpenID</h4>
            </div>
            <div class="modal-body">
                At this time, CloudSim is in beta and by invitation only. If you have received an invite, pick a login option below.
                <!--OpenID options -->
                <ul class="dropdown-menu-modal" style="list-style-type:none;">
                    <li>
                        <a href="/auth/google"><img class="inline-block"
                                                    alt="Google" width="46" src="/img/icons/google.svg"/> Google</a>
                    </li>
                    <li>
                        <a href="/auth/yahoo"><img class="inline-block"
                                                   alt="Yahoo" width="46" src="/img/icons/yahoo.svg"> Yahoo</a>
                    </li>
                    <li>
                        <a href="/auth/aol"><img class="inline-block"
                                                 alt="AOL" width="46" src="/img/icons/aol.svg"> AOL</a>
                    </li>
                    <li>
                        <a href="/auth/openid"><img class="inline-block"
                                                    alt="OpenId" width="46" src="/img/icons/openid.svg"> OpenId</a>
                    </li>
                </ul>
                <!-- end here -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
    $("#myModal").modal('show');
</script>